/*
 * ili9341_ltdc.cpp
 *
 *  Created on: 16.08.2017
 *      Author: BloedeBleidd
 */

#include "ili9341_ltdc.h"
#include <cstring>

Ili9341_LTDC::Ili9341_LTDC(const SpiInitStruct &initStruct, Color *GRAMAddress, void *VRAM1Address, Orientation _orientation, void *VRAM2Address)
: Ili9341_Spi()
{
	orientation = _orientation;
	pixelAmount = Ili9341::WIDTH_MAX * Ili9341::HEIGHT_MAX;;
	if(orientation == Portrait_1 || orientation == Portrait_2)
	{
		width = Ili9341::WIDTH_MAX;
		height = Ili9341::HEIGHT_MAX;
	}
	else
	{
		width = Ili9341::HEIGHT_MAX;
		height = Ili9341::WIDTH_MAX;
	}
	spiStruct = initStruct;
	Graphic::createBuffer(GRAMAddress, Graphic::bufferPointerGet());
	Graphic::createBuffer(VRAM1Address, VRAM1);
	if(VRAM2Address != nullptr)
		Ili9341_Spi::createBuffer(VRAM2Address, VRAM2);
	Ili9341_LTDC::hardwareInitialize();
	Ili9341_Spi::hardwareInitialize();
	Ili9341_LTDC::Ili9341Initialize();
}

Ili9341_LTDC::~Ili9341_LTDC()
{
	Graphic::deleteBuffer(VRAM2);
}

void Ili9341_LTDC::hardwareInitialize()
{
	/* PLL  */
	RCC->PLLSAICFGR = (192 << RCC_PLLSAICFGR_PLLSAIN_Pos) | (7 << RCC_PLLSAICFGR_PLLSAIQ_Pos) | (4 << RCC_PLLSAICFGR_PLLSAIR_Pos);
	RCC->DCKCFGR &= ~RCC_DCKCFGR_PLLSAIDIVR;
	RCC->DCKCFGR |= RCC_DCKCFGR_PLLSAIDIVR_1;
	/* Enable SAI PLL */
	RCC->CR |= RCC_CR_PLLSAION;
	/* wait for SAI PLL ready */
	while((RCC->CR & RCC_CR_PLLSAIRDY) == 0){};

	/* enable clock for LTDC */
	RCC->APB2ENR |= RCC_APB2ENR_LTDCEN;
	/* enable clock for DMA2D */
	RCC->AHB1ENR |= RCC_AHB1ENR_DMA2DEN;

	/* GPIO LTDC pin configuration */
	uint8_t i = 0;
	while(GPIOInitTable[i] != 0)
	{
		gpioPinConfiguration(Ili9341_LTDC::GPIOInitTable[i], Ili9341_LTDC::PINInitTable[i], GPIO_MODE_ALTERNATE_PUSH_PULL_VERY_HIGH_SPEED);
		gpioPinAlternateFunctionConfiguration(Ili9341_LTDC::GPIOInitTable[i], Ili9341_LTDC::PINInitTable[i], Ili9341_LTDC::AFInitTable[i]);
		i++;
	}

	/* disable LCD-TFT controller for re-initialisation */
	LTDC->GCR = 0;
	/* Synchronization Size Configuration */
	LTDC->SSCR = (ILI9341_HSYNC << LTDC_SSCR_HSW_Pos) | ILI9341_VSYNC;
	/* Back Porch Configuration */
	LTDC->BPCR = (ILI9341_HBP << LTDC_BPCR_AHBP_Pos) | ILI9341_VBP;
	/* Active Width Configuration */
	LTDC->AWCR = (ACTIVE_W << LTDC_AWCR_AAW_Pos) | (ACTIVE_H);
	/* Total Width Configuration */
	LTDC->TWCR = (TOTAL_WIDTH << LTDC_TWCR_TOTALW_Pos) | (TOTAL_HEIGHT);
	/* Background black */
	LTDC->BCCR = 0;
	/* Initialize layers */
	Ili9341_LTDC::layersInitialize();
	/* Enable LTDC */
	LTDC->GCR = LTDC_GCR_LTDCEN;
}

void Ili9341_LTDC::layersInitialize()
{
	/* disable for reprogramming */
	LTDC_Layer1->CR = 0;
	/* Window Horizontal Position Configuration */
	LTDC_Layer1->WHPCR = (ILI9341_HBP + 1) | ((ILI9341_HBP + ILI9341_LCD_WIDTH) << LTDC_LxWHPCR_WHSPPOS_Pos);
	/* Window Vertical Position Configuration */
	LTDC_Layer1->WVPCR = (ILI9341_VBP + 1) | ((ILI9341_VBP + ILI9341_LCD_HEIGHT) << LTDC_LxWVPCR_WVSPPOS_Pos);
	/* Pixel Format Configuration */
	LTDC_Layer1->PFCR = 2; // rgb565
	/* Color Frame Buffer Address */
	LTDC_Layer1->CFBAR = (uint32_t)VRAM1;
	/* Color Frame Buffer Length */
	LTDC_Layer1->CFBLR = ((ILI9341_LCD_WIDTH * ILI9341_PIXELWIDHT) << LTDC_LxCFBLR_CFBP_Pos) | ((ILI9341_LCD_WIDTH * ILI9341_PIXELWIDHT) + 3);
	/* Color Frame Buffer Line */
	LTDC_Layer1->CFBLNR = ILI9341_LCD_HEIGHT;
	/* Black data is transparent, allowing background to be shown */
	LTDC_Layer1->CKCR = 0x000;

	if(VRAM2 != nullptr)
	{
		/* disable for reprogramming */
		LTDC_Layer2->CR = 0;
		/* Window Horizontal Position Configuration */
		LTDC_Layer2->WHPCR = (ILI9341_HBP + 1) | ((ILI9341_HBP + ILI9341_LCD_WIDTH) << LTDC_LxWHPCR_WHSPPOS_Pos);
		/* Window Vertical Position Configuration */
		LTDC_Layer2->WVPCR = (ILI9341_VBP + 1) | ((ILI9341_VBP + ILI9341_LCD_HEIGHT) << LTDC_LxWVPCR_WVSPPOS_Pos);
		/* Pixel Format Configuration */
		LTDC_Layer2->PFCR = 2; // rgb565
		/* Color Frame Buffer Address */
		LTDC_Layer2->CFBAR = (uint32_t)VRAM2;
		/* Color Frame Buffer Length */
		LTDC_Layer2->CFBLR = ((ILI9341_LCD_WIDTH * ILI9341_PIXELWIDHT) << LTDC_LxCFBLR_CFBP_Pos) | ((ILI9341_LCD_WIDTH * ILI9341_PIXELWIDHT) + 3);
		/* Color Frame Buffer Line */
		LTDC_Layer2->CFBLNR = ILI9341_LCD_HEIGHT;
		/* Black data is transparent, allowing background to be shown */
		LTDC_Layer2->CKCR = 0x000;
	}
	Ili9341_LTDC::reloadImmediate();
}

void Ili9341_LTDC::Ili9341Initialize()
{
	/* Reset LCD */
	Ili9341_Spi::writeCmd(SOFT_RES);
	Ili9341_Spi::delay(10);
	Ili9341_Spi::writeCmd(DISPLAY_OFF);

	/* Configure LCD */
	Ili9341_Spi::writeCmd(0xCA);
	Ili9341_Spi::writeData(0xC3);
	Ili9341_Spi::writeData(0x08);
	Ili9341_Spi::writeData(0x50);
	Ili9341_Spi::writeCmd(POWERB);
	Ili9341_Spi::writeData(0x00);
	Ili9341_Spi::writeData(0xC1);
	Ili9341_Spi::writeData(0x30);
	Ili9341_Spi::writeCmd(POWER_SEQ);
	Ili9341_Spi::writeData(0x64);
	Ili9341_Spi::writeData(0x03);
	Ili9341_Spi::writeData(0x12);
	Ili9341_Spi::writeData(0x81);
	Ili9341_Spi::writeCmd(DTCA);
	Ili9341_Spi::writeData(0x85);
	Ili9341_Spi::writeData(0x00);
	Ili9341_Spi::writeData(0x78);
	Ili9341_Spi::writeCmd(POWERA);
	Ili9341_Spi::writeData(0x39);
	Ili9341_Spi::writeData(0x2C);
	Ili9341_Spi::writeData(0x00);
	Ili9341_Spi::writeData(0x34);
	Ili9341_Spi::writeData(0x02);
	Ili9341_Spi::writeCmd(PRC);
	Ili9341_Spi::writeData(0x20);
	Ili9341_Spi::writeCmd(DTCB);
	Ili9341_Spi::writeData(0x00);
	Ili9341_Spi::writeData(0x00);
	Ili9341_Spi::writeCmd(FRAME_CONT_NORM);
	Ili9341_Spi::writeData(0x00);
	Ili9341_Spi::writeData(0x1B);
	Ili9341_Spi::writeCmd(DFC);
	Ili9341_Spi::writeData(0x0A);
	Ili9341_Spi::writeData(0xA2);
	Ili9341_Spi::writeCmd(POWER1);
	Ili9341_Spi::writeData(0x10);
	Ili9341_Spi::writeCmd(POWER2);
	Ili9341_Spi::writeData(0x10);
	Ili9341_Spi::writeCmd(VCOM1);
	Ili9341_Spi::writeData(0x45);
	Ili9341_Spi::writeData(0x15);
	Ili9341_Spi::writeCmd(VCOM2);
	Ili9341_Spi::writeData(0x90);
	Ili9341_Spi::writeCmd(MAC);
	Ili9341_Spi::writeData(0xC8);
	Ili9341_Spi::writeCmd(GAMMA3_EN);
	Ili9341_Spi::writeData(0x00);
	Ili9341_Spi::writeCmd(RGB_INTERFACE);
	Ili9341_Spi::writeData(0xC2);
	Ili9341_Spi::writeCmd(DFC);
	Ili9341_Spi::writeData(0x0A);
	Ili9341_Spi::writeData(0xA7);
	Ili9341_Spi::writeData(0x27);
	Ili9341_Spi::writeData(0x04);

	/* Colomn address set */
	Ili9341_Spi::writeCmd(COLUMN_ADDR);
	Ili9341_Spi::writeData(0x00);
	Ili9341_Spi::writeData(0x00);
	Ili9341_Spi::writeData(0x00);
	Ili9341_Spi::writeData(0xEF);
	/* Page address set */
	Ili9341_Spi::writeCmd(PAGE_ADDR);
	Ili9341_Spi::writeData(0x00);
	Ili9341_Spi::writeData(0x00);
	Ili9341_Spi::writeData(0x01);
	Ili9341_Spi::writeData(0x3F);
	Ili9341_Spi::writeCmd(INTERFACE);
	Ili9341_Spi::writeData(0x01);
	Ili9341_Spi::writeData(0x00);
	Ili9341_Spi::writeData(0x06);

	Ili9341_Spi::writeCmd(GRAM);
	Ili9341_Spi::delay(200);

	Ili9341_Spi::writeCmd(GAMMA);
	Ili9341_Spi::writeData(0x01);

	Ili9341_Spi::writeCmd(PGAMMA);
	Ili9341_Spi::writeData(0x0F);
	Ili9341_Spi::writeData(0x29);
	Ili9341_Spi::writeData(0x24);
	Ili9341_Spi::writeData(0x0C);
	Ili9341_Spi::writeData(0x0E);
	Ili9341_Spi::writeData(0x09);
	Ili9341_Spi::writeData(0x4E);
	Ili9341_Spi::writeData(0x78);
	Ili9341_Spi::writeData(0x3C);
	Ili9341_Spi::writeData(0x09);
	Ili9341_Spi::writeData(0x13);
	Ili9341_Spi::writeData(0x05);
	Ili9341_Spi::writeData(0x17);
	Ili9341_Spi::writeData(0x11);
	Ili9341_Spi::writeData(0x00);
	Ili9341_Spi::writeCmd(NGAMMA);
	Ili9341_Spi::writeData(0x00);
	Ili9341_Spi::writeData(0x16);
	Ili9341_Spi::writeData(0x1B);
	Ili9341_Spi::writeData(0x04);
	Ili9341_Spi::writeData(0x11);
	Ili9341_Spi::writeData(0x07);
	Ili9341_Spi::writeData(0x31);
	Ili9341_Spi::writeData(0x33);
	Ili9341_Spi::writeData(0x42);
	Ili9341_Spi::writeData(0x05);
	Ili9341_Spi::writeData(0x0C);
	Ili9341_Spi::writeData(0x0A);
	Ili9341_Spi::writeData(0x28);
	Ili9341_Spi::writeData(0x2F);
	Ili9341_Spi::writeData(0x0F);

	Ili9341_Spi::writeCmd(SLEEP_OUT);
	Ili9341_Spi::delay(100);
	Ili9341_Spi::writeCmd(DISPLAY_ON);
	Ili9341_Spi::delay(100);
	/* GRAM start writing */
	Ili9341_Spi::writeCmd(GRAM);
}

bool Ili9341_LTDC::layerSet(Layer l)
{
	if(l == TOP)
	{
		if(isLayerTopUsed())
		{
			layerCurrent = l;
			return true;
		}
		return false;
	}
	layerCurrent = l;
	return true;
}

bool Ili9341_LTDC::layerChange()
{
	if(layerCurrent == BOTTOM)
	{
		if(isLayerTopUsed())
		{
			layerCurrent = TOP;
			return true;
		}
		return false;
	}
	layerCurrent = BOTTOM;
	return true;
}

bool Ili9341_LTDC::refreshLayer(Layer l)
{
	if(l == TOP)
		return Ili9341_LTDC::refreshTop();
	Ili9341_LTDC::refreshBottom();
	return true;
}

bool Ili9341_LTDC::refreshTop()
{
	if(!isLayerTopUsed())
		return false;
	Ili9341_LTDC::buffCopy32Bit(VRAM2, Graphic::bufferPointerGet(), pixelAmount/sizeof(Color));
	//memcpy(VRAM2,Graphic::bufferPointerGet(),pixelAmount*sizeof(Color));
	return true;
}

void Ili9341_LTDC::refreshBottom()
{
	Ili9341_LTDC::buffCopy32Bit(VRAM1, Graphic::bufferPointerGet(), pixelAmount/sizeof(Color));
	//memcpy(VRAM1,Graphic::bufferPointerGet(),pixelAmount*sizeof(Color));
}

void Ili9341_LTDC::refreshCurrentLayer()
{
	if(layerCurrent == TOP)
		Ili9341_LTDC::refreshTop();
	else
		Ili9341_LTDC::refreshBottom();
}

bool Ili9341_LTDC::refreshNotCurrentLayer()
{
	if(layerCurrent == BOTTOM)
		if(isLayerTopUsed())
			return Ili9341_LTDC::refreshTop();
	Ili9341_LTDC::refreshBottom();
	return true;
}

bool Ili9341_LTDC::layerOpacitySetTop(uint8_t o)
{
	if(!isLayerTopUsed())
		return false;
	LTDC_Layer2->CACR = o;
	Ili9341_LTDC::reloadImmediate();
	return true;
}

void Ili9341_LTDC::layerOpacitySetBottom(uint8_t o)
{
	LTDC_Layer1->CACR = o;
	Ili9341_LTDC::reloadImmediate();
}

void Ili9341_LTDC::layerOpacitySetCurrent(uint8_t o)
{
	if(layerCurrent== BOTTOM)
		Ili9341_LTDC::layerOpacitySetTop(o);
	Ili9341_LTDC::layerOpacitySetBottom(o);

}

bool Ili9341_LTDC::layerOpacitySetNotCurrent(uint8_t o)
{
	if(layerCurrent== BOTTOM)
		return Ili9341_LTDC::layerOpacitySetTop(o);
	Ili9341_LTDC::layerOpacitySetBottom(o);
	return true;
}

bool Ili9341_LTDC::layerOpacityGetTop(uint8_t &o)
{
	if(!isLayerTopUsed())
		return false;
	o = LTDC_Layer2->CACR;
	return true;
}

void Ili9341_LTDC::layerOpacityGetBottom(uint8_t &o)
{
	o = LTDC_Layer1->CACR;
}

void Ili9341_LTDC::layerOpacityGetCurrent(uint8_t &o)
{
	if(layerCurrent== BOTTOM)
		Ili9341_LTDC::layerOpacityGetTop(o);
	Ili9341_LTDC::layerOpacityGetBottom(o);

}

bool Ili9341_LTDC::layerOpacityGetNotCurrent(uint8_t &o)
{
	if(layerCurrent== TOP)
		return Ili9341_LTDC::layerOpacityGetTop(o);
	Ili9341_LTDC::layerOpacityGetBottom(o);
	return true;
}

bool Ili9341_LTDC::layerCopyFromTopToBottom()
{
	if(!isLayerTopUsed())
		return false;
	uint32_t len = width*height;
	for(uint32_t i=0; i<len; i++)
		VRAM1[i] = VRAM2[i];
	return true;
}

bool Ili9341_LTDC::layerCopyFromBottomToTop()
{
	if(!isLayerTopUsed())
		return false;
	uint32_t len = width*height;
	for(uint32_t i=0; i<len; i++)
		VRAM2[i] = VRAM1[i];
	return true;
}

bool Ili9341_LTDC::layerCopyFromCurrent()
{
	if(!isLayerTopUsed())
		return false;
	if(layerCurrent== TOP)
		Ili9341_LTDC::layerCopyFromTopToBottom();
	else
		Ili9341_LTDC::layerCopyFromBottomToTop();
	return true;
}

bool Ili9341_LTDC::layerCopyToCurrent()
{
	if(!isLayerTopUsed())
		return false;
	if(layerCurrent== TOP)
		Ili9341_LTDC::layerCopyFromBottomToTop();
	else
		Ili9341_LTDC::layerCopyFromTopToBottom();
	return true;
}

bool Ili9341_LTDC::layerOn(Layer l)
{
	if(l == TOP)
	{
		if(isLayerTopUsed())
		{
			Ili9341_LTDC::layerOnTop();
			return true;
		}
		return false;
	}
	Ili9341_LTDC::layerOnBottom();
	return true;
}

bool Ili9341_LTDC::layerOnTop()
{
	if(!isLayerTopUsed())
		return false;
	LTDC_Layer2->CR |= LTDC_LxCR_LEN;
	Ili9341_LTDC::reloadVerticalBlanking();
	return true;
}

void Ili9341_LTDC::layerOnBottom()
{
	LTDC_Layer1->CR |= LTDC_LxCR_LEN;
	Ili9341_LTDC::reloadVerticalBlanking();
}

void Ili9341_LTDC::layerOnCurrent()
{
	if(layerCurrent== BOTTOM)
		Ili9341_LTDC::layerOnBottom();
	else
		Ili9341_LTDC::layerOnTop();
}

bool Ili9341_LTDC::layerOnNotCurrent()
{
	if(layerCurrent== BOTTOM)
		return Ili9341_LTDC::layerOnTop();
	Ili9341_LTDC::layerOnBottom();
	return true;
}

bool Ili9341_LTDC::layerOff(Layer l)
{
	if(l == TOP)
	{
		if(isLayerTopUsed())
		{
			Ili9341_LTDC::layerOffTop();
			return true;
		}
		return false;
	}
	Ili9341_LTDC::layerOffBottom();
	return true;
}

bool Ili9341_LTDC::layerOffTop()
{
	if(!isLayerTopUsed())
		return false;
	LTDC_Layer2->CR &= ~LTDC_LxCR_LEN;
	Ili9341_LTDC::reloadVerticalBlanking();
	return true;
}

void Ili9341_LTDC::layerOffBottom()
{
	LTDC_Layer1->CR &= ~LTDC_LxCR_LEN;
	Ili9341_LTDC::reloadVerticalBlanking();
}

void Ili9341_LTDC::layerOffCurrent()
{
	if(layerCurrent== BOTTOM)
		Ili9341_LTDC::layerOffBottom();
	else
		Ili9341_LTDC::layerOffTop();
}

bool Ili9341_LTDC::layerOffNotCurrent()
{
	if(layerCurrent== BOTTOM)
		return Ili9341_LTDC::layerOffTop();
	Ili9341_LTDC::layerOffBottom();
	return true;
}

void Ili9341_LTDC::displayOn()
{
	/* Send command to display on */
	Ili9341_Spi::writeCmd(DISPLAY_ON);
	/* Enable LTDC */
	LTDC->GCR |= LTDC_GCR_LTDCEN;
}

void Ili9341_LTDC::displayOff()
{
	/* Send command to display on */
	Ili9341_Spi::writeCmd(DISPLAY_OFF);
	/* Enable LTDC */
	LTDC->GCR &= ~LTDC_GCR_LTDCEN;
}

void Ili9341_LTDC::buffCopy32Bit(void *dst, void const *src, uint32_t len)
{
	uint32_t * tmpDst = (uint32_t *) dst;
	uint32_t const * tmpSrc = (uint32_t const *) src;

	while(len)
	{
		*tmpDst++ = *tmpSrc++;
		len--;
	}
}

/*
 * graphic.cpp
 *
 *  Created on: 05.07.2017
 *      Author: BloedeBleidd
 */

#include "graphic.h"
#include <cstring>
#include <algorithm>
#include <cmath>
#include <iomanip>


void Graphic::createBuffer(void *address, Color *&buffer)
{
	if(address != nullptr)
		buffer = new (address) Color [pixelAmount];
	else
		buffer = new Color [pixelAmount];
	memset(buffer,0,pixelAmount*sizeof(Color));
}

void Graphic::deleteBuffer(Color *buffer)
{
	delete[] buffer;
	buffer = nullptr;
}

void Graphic::drawPixel( int32_t x, int32_t y, uint16_t color )
{
	if( (x < 0) || (x >= width) || (y < 0) || (y >= height) )	{ return; }

	switch(orientation)
	{
	case Landscape_2:
		*(Color *)(graphicBuffer + ( (y-height)+(width-x)*height) ) = color;
	break;
	case Landscape_1:
		*(Color *)(graphicBuffer + ( ((x+1)*height)+(height-(y+1))-height )) = color;
	break;
	case Portrait_2:
		*(Color *)(graphicBuffer + (pixelAmount - x - width*y) ) = color;
	break;
	case Portrait_1:
		*(Color *)(graphicBuffer + (x + width*y)) = color;
	break;
	}
}

void Graphic::drawLine( int32_t xBeg, int32_t yBeg, int32_t xEnd, int32_t yEnd, Color color )
{
	int32_t dx, dy;
	bool steep = false;
	int32_t ystep;
	int32_t err;

	if( ( abs( yEnd - yBeg ) ) > ( abs( xEnd - xBeg ) ) )	steep = true;

	if( true == steep )
	{
		std::swap( xBeg, yBeg );
		std::swap( xEnd, yEnd );
	}

	if( xBeg > xEnd )
	{
		std::swap( xBeg, xEnd );
		std::swap( yBeg, yEnd );
	}

	dx = xEnd - xBeg;
	dy = abs( yEnd - yBeg );
	err = dx / 2;

	if( yBeg < yEnd )
	{
		ystep = 1;
	}
	else
	{
		ystep = -1;
	}

	for( ; xBeg <= xEnd; xBeg++ )
	{
		if( true == steep )
		{
			Graphic::drawPixel( yBeg, xBeg, color );
		}
		else
		{
			Graphic::drawPixel( xBeg, yBeg, color );
		}

		err -= dy;

		if( err < 0 )
		{
			yBeg += ystep;
			err += dx;
		}
	}
}

void Graphic::drawLineVertical( int32_t x, int32_t yBeg, int32_t yEnd, Color color )
{
	if( yBeg < yEnd )
	{
		for( int32_t i=yBeg; i<yEnd; i++ )
		{
			Graphic::drawPixel( x, i, color );
		}
	}
	else
	{
		for( int32_t i=yBeg; i>yEnd; i-- )
		{
			Graphic::drawPixel( x, i, color );
		}
	}
}

void Graphic::drawLineHorizontal( int32_t xBeg, int32_t xEnd, int32_t y, Color color )
{
	if( xBeg < xEnd )
	{
		for( int32_t i=xBeg; i<xEnd; i++ )
		{
			Graphic::drawPixel( i, y, color );
		}
	}
	else
	{
		for( int32_t i=xBeg; i>xEnd; i-- )
		{
			Graphic::drawPixel( i, y, color );
		}
	}
}

void Graphic::drawRectangle( int32_t xStart, int32_t yStart, int32_t xEnd, int32_t yEnd, Color color )
{
	Graphic::drawLineVertical( xStart, yStart, yEnd, color );
	Graphic::drawLineVertical( xEnd, yStart, yEnd, color );
	Graphic::drawLineHorizontal( xStart, xEnd, yStart, color );
	Graphic::drawLineHorizontal( xStart, xEnd, yEnd, color );
}

void Graphic::drawRectangleAndFill( int32_t xStart, int32_t yStart, int32_t xEnd, int32_t yEnd, Color color )
{
	for( int32_t i=xStart; i<xEnd; i++ )
	{
		Graphic::drawLineVertical( i, yStart, yEnd, color );
	}
}

void Graphic::drawRectangleWithRoundedEdges( int32_t xStart, int32_t yStart, int32_t xEnd, int32_t yEnd, uint16_t r, Color color )
{
	/* Check input parameters */
	if (xEnd == xStart || yEnd == yStart)
		return;

	/* No radius */
	if (r == 0)
	{
		Graphic::drawRectangle(xStart, yStart, xEnd, yEnd, color);
		return;
	}

	/* Swap X coordinates */
	if (xStart > xEnd)
		std::swap(xStart, xEnd);

	/* Swap Y coordinates */
	if (yStart > yEnd)
		std::swap(yStart, yEnd);

	/* Check max radius */
	if (r > ((xEnd - xStart) / 2))
		r = (xEnd - xStart) / 2;
	if (r > ((yEnd - yStart) / 2))
		r = (yEnd - yStart) / 2;

	/* Draw lines */
	Graphic::drawLine(xStart + r, yStart, xEnd - r, yStart, color); /* Top */
	Graphic::drawLine(xStart, yStart + r, xStart, yEnd - r, color);	/* Left */
	Graphic::drawLine(xEnd, yStart + r, xEnd, yEnd - r, color);	/* Right */
	Graphic::drawLine(xStart + r, yEnd, xEnd - r, yEnd, color);	/* Bottom */

	/* Draw corners */
	Graphic::drawCircleCorner(xStart + r, yStart + r, r, FIRST, color); /* Top left */
	Graphic::drawCircleCorner(xEnd - r, yStart + r, r, SECOND, color); /* Top right */
	Graphic::drawCircleCorner(xEnd - r, yEnd - r, r, THIRD, color); /* Bottom right */
	Graphic::drawCircleCorner(xStart + r, yEnd - r, r, FOURTH, color); /* Bottom left */
}

void Graphic::drawRectangleWithRoundedEdgesAndFill( int32_t xStart, int32_t yStart, int32_t xEnd, int32_t yEnd, uint16_t r, Color color )
{
	/* Check input parameters */
	if (xEnd == xStart || yEnd == yStart)
		return;

	/* No radius */
	if (r == 0)
	{
		Graphic::drawRectangleAndFill(xStart, yStart, xEnd, yEnd, color);
		return;
	}

	/* Swap X coordinates */
	if (xStart > xEnd)
		std::swap(xStart, xEnd);

	/* Swap Y coordinates */
	if (yStart > yEnd)
		std::swap(yStart, yEnd);

	/* Check max radius */
	if (r > ((xEnd - xStart) / 2))
		r = (xEnd - xStart) / 2;
	if (r > ((yEnd - yStart) / 2))
		r = (yEnd - yStart) / 2;

	/* Draw rectangles */
	Graphic::drawRectangleAndFill(xStart + r, yStart, xEnd - r, yEnd, color);
	Graphic::drawRectangleAndFill(xStart, yStart + r, xStart + r, yEnd - r, color);
	Graphic::drawRectangleAndFill(xEnd - r, yStart + r, xEnd, yEnd - r, color);

	/* Draw corners */
	Graphic::drawCircleCornerAndFill(xStart + r, yStart + r, r, FIRST, color);
	Graphic::drawCircleCornerAndFill(xEnd - r, yStart + r, r, SECOND, color);
	Graphic::drawCircleCornerAndFill(xEnd - r, yEnd - r - 1, r, THIRD, color);
	Graphic::drawCircleCornerAndFill(xStart + r, yEnd - r - 1, r, FOURTH, color);
}

void Graphic::drawCircle( int32_t x, int32_t y, uint16_t r, Color color )
{
	int16_t f = 1 - r;
	int16_t ddF_x = 1;
	int16_t ddF_y = -2 * r;
	int16_t xTmp = 0;
	int16_t yTmp = r;

	Graphic::drawPixel(x, y + r, color);
    Graphic::drawPixel(x, y - r, color);
    Graphic::drawPixel(x + r, y, color);
    Graphic::drawPixel(x - r, y, color);

    while (xTmp < yTmp)
    {
        if (f >= 0)
        {
        	yTmp--;
            ddF_y += 2;
            f += ddF_y;
        }

        xTmp++;
        ddF_x += 2;
        f += ddF_x;

        Graphic::drawPixel(x + xTmp, y + yTmp, color);
        Graphic::drawPixel(x - xTmp, y + yTmp, color);
        Graphic::drawPixel(x + xTmp, y - yTmp, color);
        Graphic::drawPixel(x - xTmp, y - yTmp, color);

        Graphic::drawPixel(x + yTmp, y + xTmp, color);
        Graphic::drawPixel(x - yTmp, y + xTmp, color);
        Graphic::drawPixel(x + yTmp, y - xTmp, color);
        Graphic::drawPixel(x - yTmp, y - xTmp, color);
    }
}

void Graphic::drawCircleAndFill( int32_t x, int32_t y, uint16_t r, Color color )
{
	uint16_t f = 1 - r;
	int16_t ddF_x = 1;
	int16_t ddF_y = -2 * r;
	int16_t xTmp = 0;
	int16_t yTmp = r;

	Graphic::drawPixel(x, y + r, color);
	Graphic::drawPixel(x, y - r, color);
    Graphic::drawPixel(x + r, y, color);
    Graphic::drawPixel(x - r, y, color);
    Graphic::drawLine(x - r, y, x + r, y, color);

	while (xTmp < yTmp)
	{
		if (f >= 0)
		{
			yTmp--;
			ddF_y += 2;
			f += ddF_y;
		}

		xTmp++;
		ddF_x += 2;
		f += ddF_x;

		Graphic::drawLine(x - xTmp, y + yTmp, x + xTmp, y + yTmp, color);
		Graphic::drawLine(x + xTmp, y - yTmp, x - xTmp, y - yTmp, color);
		Graphic::drawLine(x + yTmp, y + xTmp, x - yTmp, y + xTmp, color);
		Graphic::drawLine(x + yTmp, y - xTmp, x - yTmp, y - xTmp, color);
	}
}

void Graphic::drawCircleCorner( int32_t x, int32_t y, uint16_t r, Corner corner, Color color )
{
	int16_t f = 1 - r;
	int16_t ddF_x = 1;
	int16_t ddF_y = -2 * r;
	int16_t xTmp = 0;
	int16_t yTmp = r;

	while (xTmp < yTmp)
	{
		if (f >= 0)
		{
			yTmp--;
			ddF_y += 2;
			f += ddF_y;
		}

		xTmp++;
		ddF_x += 2;
		f += ddF_x;

		if (corner & 0x1)
		{
			Graphic::drawPixel(x - yTmp, y - xTmp, color);
			Graphic::drawPixel(x - xTmp, y - yTmp, color);
		}

		if (corner & 0x2)
		{
			Graphic::drawPixel(x + xTmp, y - yTmp, color);
			Graphic::drawPixel(x + yTmp, y - xTmp, color);
		}

		if (corner & 0x4)
		{
			Graphic::drawPixel(x + xTmp, y + yTmp, color);
			Graphic::drawPixel(x + yTmp, y + xTmp, color);
		}

		if (corner & 0x8)
		{
			Graphic::drawPixel(x - xTmp, y + yTmp, color);
			Graphic::drawPixel(x - yTmp, y + xTmp, color);
		}
	}
}

void Graphic::drawCircleCornerAndFill( int32_t x, int32_t y, uint16_t r, Corner corner, Color color )
{
	int16_t f = 1 - r;
	int16_t ddF_x = 1;
	int16_t ddF_y = -2 * r;
	int16_t xTmp = 0;
	int16_t yTmp = r;

	while (xTmp < yTmp)
	{
		if (f >= 0)
		{
			yTmp--;
			ddF_y += 2;
			f += ddF_y;
		}

		xTmp++;
		ddF_x += 2;
		f += ddF_x;

		if (corner & 0x1)
		{
			Graphic::drawLine(x, y - yTmp, x - xTmp, y - yTmp, color);
			Graphic::drawLine(x, y - xTmp, x - yTmp, y - xTmp, color);
		}

		if (corner & 0x2)
		{
			Graphic::drawLine(x + xTmp, y - yTmp, x, y - yTmp, color);
			Graphic::drawLine(x + yTmp, y - xTmp, x, y - xTmp, color);
		}

		if (corner & 0x4)
		{
			Graphic::drawLine(x, y + yTmp, x + xTmp, y + yTmp, color);
			Graphic::drawLine(x + yTmp, y + xTmp, x, y + xTmp, color);
		}

		if (corner & 0x8)
		{
			Graphic::drawLine(x - xTmp, y + yTmp, x, y + yTmp, color);
			Graphic::drawLine(x, y + xTmp, x - yTmp, y + xTmp, color);
		}
	}
}

void Graphic::drawTriangle(int32_t xA, int32_t yA, int32_t xB, int32_t yB, int32_t xC, int32_t yC, Color color)
{
	Graphic::drawLine(xA, yA, xB, yB, color);
	Graphic::drawLine(xB, yB, xC, yC, color);
	Graphic::drawLine(xA, yA, xC, yC, color);
}

void Graphic::drawTriangleAndFill(int32_t xA, int32_t yA, int32_t xB, int32_t yB, int32_t xC, int32_t yC, Color color)
{
	using std::swap;
    int16_t a, b, y, last;

    // Sort coordinates by Y order (yC >= yB >= yA)
    if (yA > yB)
    {
        swap(yA, yB);
        swap(xA, xB);
    }
    if (yB > yC)
    {
        swap(yC, yB);
        swap(xC, xB);
    }
    if (yA > yB)
    {
        swap(yA, yB);
        swap(xA, xB);
    }

    if(yA == yC)
    { // Handle awkward all-on-same-line case as its own thing
        a = b = xA;
        if(xB < a)      a = xB;
        else if(xB > b) b = xB;
        if(xC < a)      a = xC;
        else if(xC > b) b = xC;
        Graphic::drawLineHorizontal(a, b, yA, color);
        return;
    }

    int16_t
    dxA1 = xB - xA,
    dyA1 = yB - yA,
    dxA2 = xC - xA,
    dyA2 = yC - yA,
    dxB2 = xC - xB,
    dyB2 = yC - yB;
    int32_t
    sa   = 0,
    sb   = 0;

    // For upper part of triangle, find scanline crossings for segments
    // 0-1 and 0-2.  If yB=yC (flat-bottomed triangle), the scanline yB
    // is included here (and second loop will be skipped, avoiding a /0
    // error there), otherwise scanline yB is skipped here and handled
    // in the second loop...which also avoids a /0 error here if yA=yB
    // (flat-topped triangle).
    if(yB == yC) last = yB;   // Include yB scanline
    else         last = yB-1; // Skip it

    for(y=yA; y<=last; y++)
    {
        a   = xA + sa / dyA1;
        b   = xA + sb / dyA2;
        sa += dxA1;
        sb += dxA2;
        /* longhand:
        a = xA + (xB - xA) * (y - yA) / (yB - yA);
        b = xA + (xC - xA) * (y - yA) / (yC - yA);
        */
        if(a > b) swap(a,b);
        Graphic::drawLineHorizontal(a, b, y, color);
    }

    // For lower part of triangle, find scanline crossings for segments
    // 0-2 and 1-2.  This loop is skipped if yB=yC.
    sa = dxB2 * (y - yB);
    sb = dxA2 * (y - yA);
    for(; y<=yC; y++)
    {
        a   = xB + sa / dyB2;
        b   = xA + sb / dyA2;
        sa += dxB2;
        sb += dxA2;
        /* longhand:
        a = xB + (xC - xB) * (y - yB) / (yC - yB);
        b = xA + (xC - xA) * (y - yA) / (yC - yA);
        */
        if(a > b) swap(a,b);
        Graphic::drawLineHorizontal(a, b, y, color);
    }
}

void Graphic::drawChar(int32_t x, int32_t y, const char ch, Color color )
{
	uint32_t i, j, b;

	xCursor = x;
	yCursor = y;

	if ((x + font.FontWidth) > width)
	{/* If at the end of a line of display, go to new line and set x to 0 position */
		yCursor += font.FontHeight;
		xCursor = 0;
	}

	for ( i = 0; i<(font.FontHeight); i++ )
	{
		b = font.data[(ch - 32) * (font.FontHeight) + i];

		for ( j = 0; j<(font.FontWidth); j++ )
		{
			if ( (b << j) & 0x8000 )
			{
				Graphic::drawPixel( xCursor + j, (yCursor + i), color );
			}
		}
	}

	/* Increase pointer */
	xCursor += font.FontWidth;
}

void Graphic::drawString(int32_t x, int32_t y, const char *str, Color color )
{
	xCursor = x;
	yCursor = y;

	while (*str)
	{
		// New line
		if (*str == '\n')
		{
			yCursor += font.FontHeight + 1;
			// if after \n is also \r, than go to the left of the screen
			if (*(str + 1) == '\r')
			{
				xCursor = 0;
				str++;
			}
			// else xCursor equals initial x value
			else
			{
				xCursor = x;
			}
			str++;
			continue;
		}
		else if (*str == '\r')
		{
			str++;
			continue;
		}
		Graphic::drawChar( xCursor, yCursor, *str++, color );
	}
}

void Graphic::drawString(int32_t x, int32_t y, const std::string &str, Color color )
{
	Graphic::drawString(x, y, str.c_str(), color);
}

void Graphic::drawInteger(int32_t x, int32_t y, int32_t data, Color color )
{
	char buf [32];
	itoa(data, buf, integerBase);
	Graphic::drawString( x, y, buf, color );
}

void Graphic::drawFloat(int32_t x, int32_t y, float data, Color color )
{
	char buf [32];
	itoa(data, buf, floatPrecision);
	Graphic::drawString( x, y, buf, color );
}

void Graphic::drawData(int32_t x, int32_t y, const char ch, Color color )
{
	Graphic::drawChar(x, y, ch, color);
}

void Graphic::drawData(int32_t x, int32_t y, const char *str, Color color )
{
	Graphic::drawString(x, y, str, color);
}

void Graphic::drawData(int32_t x, int32_t y, const std::string &str, Color color )
{
	Graphic::drawString(x, y, str, color);
}

void Graphic::drawData(int32_t x, int32_t y, int32_t data, Color color )
{
	Graphic::drawInteger(x, y, data, color);
}

void Graphic::drawData(int32_t x, int32_t y, float data, Color color )
{
	Graphic::drawFloat(x, y, data, color);
}

void Graphic::drawBitmap16Bit(int32_t x, int32_t y, const Color *pixel, uint16_t _width, uint16_t _height)
{
	for(int32_t i=y; i<_height+y; i++)
		for(int32_t j=x; j<_width+x; j++)
			Graphic::drawPixel(j, i, *pixel++);
}

void Graphic::fill( Color color )
{
	uint32_t pixels = height*width;

	for(uint32_t i=0; i<pixels; i++) { graphicBuffer[i] = color; }
}


void Graphic::ftoa(float f, char *str, uint8_t precision)
{
	uint8_t i, j, divisor = 1;
	int8_t log_f;
	int32_t int_digits = (int)f;             //store the integer digits
	float decimals;
	char s1[12];

	memset(str, 0, sizeof(str)+1);
	memset(s1, 0, 10);

	if (f < 0)
	{										//if a negative number
		str[0] = '-';						//start the char array with '-'
		f = abs(f);							//store its positive absolute value
	}
	log_f = ceil(log10f(f));					//get number of digits before the decimal
	if (log_f > 0)
	{										//log value > 0 indicates a number > 1
		if (log_f == precision)
		{             							 //if number of digits = significant figures
			f += 0.5;                            //add 0.5 to round up decimals >= 0.5
			itoa(f, s1, 10);                     //itoa converts the number to a char array
			strcat(str, s1);                     //add to the number string
		}
		else if ((log_f - precision) > 0)
		{    										//if more integer digits than significant digits
			i = log_f - precision;               //count digits to discard
			divisor = 10;
			for (j = 0; j < i; j++) divisor *= 10;    //divisor isolates our desired integer digits
			f /= divisor;                             //divide
			f += 0.5;                            //round when converting to int
			int_digits = (int)f;
			int_digits *= divisor;               //and multiply back to the adjusted value
			itoa(int_digits, s1, 10);
			strcat(str, s1);
		}
		else
		{                                		 //if more precision specified than integer digits,
			itoa(int_digits, s1, 10);            //convert
			strcat(str, s1);                     //and append
		}
	}
	else
	{                                  			 //decimal fractions between 0 and 1: leading 0
		s1[0] = '0';
		strcat(str, s1);
	}

	if (log_f < precision)
	{                							 //if precision exceeds number of integer digits,
		decimals = f - (int)f;                 //get decimal value as float
		strcat(str, ".");                      //append decimal point to char array
		i = precision - log_f;                 //number of decimals to read

		for (j = 0; j < i; j++)
		{             							 //for each,
			decimals *= 10;                      //multiply decimals by 10
			if (j == (i-1)) decimals += 0.5;     //and if it's the last, add 0.5 to round it
			itoa((int)decimals, s1, 10);         //convert as integer to character array
			strcat(str, s1);                     //append to string
			decimals -= (int)decimals;           //and remove, moving to the next
		}
	}
}


/*
 * Ili9341_Spi.cpp
 *
 *  Created on: 05.07.2017
 *      Author: BloedeBleidd
 */

#include "ili9341_spi.h"


Ili9341_Spi::Ili9341_Spi(uint16_t _width, uint16_t _height, const SpiInitStruct &initStruct, Orientation _orientation, void *_buffAddress)
{
	width = _width;
	height = _height;
	pixelAmount = width*height;
	orientation = _orientation;
	spiStruct = initStruct;
	Graphic::createBuffer(_buffAddress, Graphic::bufferPointerGet());
	Graphic::bufferPointerSet(Graphic::bufferPointerGet());
	Graphic::orientationSet(orientation);
	Ili9341_Spi::hardwareInitialize();
	Ili9341_Spi::Ili9341Initialize();
}

Ili9341_Spi::~Ili9341_Spi()
{
	Graphic::deleteBuffer(Graphic::bufferPointerGet());
}

void Ili9341_Spi::csControl( CsControl state )
{
	if( state == SELECT )
	{
		gpioBitReset(spiStruct.gpioChipSelect, spiStruct.chipSelect);
	}
	else
	{
		gpioBitSet(spiStruct.gpioChipSelect, spiStruct.chipSelect);
	}
}

void Ili9341_Spi::dcControl(DcControl state)
{
	if(state == COMMAND)
	{
		gpioBitReset(spiStruct.gpioDataCommand, spiStruct.dataCommand);
	}
	else
	{
		gpioBitSet(spiStruct.gpioDataCommand, spiStruct.dataCommand);
	}
}

void Ili9341_Spi::spiFastMode()
{
	spiSetPrescaler(spiStruct.spi, SPI_PRESCALER_2);
}

void Ili9341_Spi::spiSlowMode()
{
	spiSetPrescaler(spiStruct.spi, SPI_PRESCALER_256);
}

void Ili9341_Spi::delay(uint16_t del)
{
	vTaskDelay(pdMS_TO_TICKS( del ));
}

void Ili9341_Spi::writeByte( Ili9341RegisterSize byte)
{
	Ili9341_Spi::csControl( SELECT );
	spiSetMode8Bit(spiStruct.spi);
	spiExchangeData(spiStruct.spi, byte);
	Ili9341_Spi::csControl( DESELECT );
}

void Ili9341_Spi::writeCmd( Ili9341RegisterSize cmd)
{
	Ili9341_Spi::dcControl( COMMAND );
	Ili9341_Spi::writeByte( cmd );
}

void Ili9341_Spi::writeData( Ili9341RegisterSize data)
{
	Ili9341_Spi::dcControl( DATA );
	Ili9341_Spi::writeByte( data );
}

void Ili9341_Spi::hardwareInitialize()
{
	// SCK, MOSI, DC, CS
	gpioPinConfiguration(spiStruct.gpioMosi, spiStruct.mosi,				GPIO_MODE_ALTERNATE_PUSH_PULL_HIGH_SPEED);
	gpioPinAlternateFunctionConfiguration( spiStruct.gpioMosi, spiStruct.mosi, spiStruct.altFunction );
	gpioPinConfiguration(spiStruct.gpioClock, spiStruct.clock,				GPIO_MODE_ALTERNATE_PUSH_PULL_HIGH_SPEED);
	gpioPinAlternateFunctionConfiguration( spiStruct.gpioClock, spiStruct.clock, spiStruct.altFunction );
	gpioPinConfiguration(spiStruct.gpioChipSelect, spiStruct.chipSelect,	GPIO_MODE_OUTPUT_PUSH_PULL_HIGH_SPEED);
	gpioPinConfiguration(spiStruct.gpioDataCommand, spiStruct.dataCommand,	GPIO_MODE_OUTPUT_PUSH_PULL_HIGH_SPEED);

	// init spi
	spiStruct.spi->CR1 = SPI_CR1_MSTR | SPI_CR1_SSM | SPI_CR1_SSI;
	Ili9341_Spi::spiSlowMode();
	spiSetMode8Bit(spiStruct.spi);
	spiEnable(spiStruct.spi);
}

void Ili9341_Spi::Ili9341Initialize()
{
	//Start initial Sequence
	Ili9341_Spi::writeCmd( SOFT_RES ); //software reset
	Ili9341_Spi::delay(10);
	Ili9341_Spi::writeCmd( DISPLAY_OFF ); // display off

	Ili9341_Spi::writeCmd( POWERA );
	Ili9341_Spi::writeData(0x39);
	Ili9341_Spi::writeData(0x2C);
	Ili9341_Spi::writeData(0x00);
	Ili9341_Spi::writeData(0x34);
	Ili9341_Spi::writeData(0x02);
	Ili9341_Spi::writeCmd( POWERB );
	Ili9341_Spi::writeData(0x00);
	Ili9341_Spi::writeData(0xC1);
	Ili9341_Spi::writeData(0x30);
	Ili9341_Spi::writeCmd( DTCA );
	Ili9341_Spi::writeData(0x85);
	Ili9341_Spi::writeData(0x00);
	Ili9341_Spi::writeData(0x78);
	Ili9341_Spi::writeCmd( DTCB );
	Ili9341_Spi::writeData(0x00);
	Ili9341_Spi::writeData(0x00);
	Ili9341_Spi::writeCmd( POWER_SEQ );
	Ili9341_Spi::writeData(0x64);
	Ili9341_Spi::writeData(0x03);
	Ili9341_Spi::writeData(0x12);
	Ili9341_Spi::writeData(0x81);
	Ili9341_Spi::writeCmd( PRC );
	Ili9341_Spi::writeData(0x20);
	Ili9341_Spi::writeCmd( POWER1 );
	Ili9341_Spi::writeData(0x23);
	Ili9341_Spi::writeCmd( POWER2 );
	Ili9341_Spi::writeData(0x10);
	Ili9341_Spi::writeCmd( VCOM1 );
	Ili9341_Spi::writeData(0x3E);
	Ili9341_Spi::writeData(0x28);
	Ili9341_Spi::writeCmd( VCOM2 );
	Ili9341_Spi::writeData(0x86);
	Ili9341_Spi::writeCmd( MAC );
	Ili9341_Spi::writeData(0x48);
	Ili9341_Spi::writeCmd( PIXEL_FORMAT );
	Ili9341_Spi::writeData(0x55);
	Ili9341_Spi::writeCmd( FRC );
	Ili9341_Spi::writeData(0x00);
	Ili9341_Spi::writeData(0x18);
	Ili9341_Spi::writeCmd( DFC );
	Ili9341_Spi::writeData(0x08);
	Ili9341_Spi::writeData(0x82);
	Ili9341_Spi::writeData(0x27);
	Ili9341_Spi::writeCmd( GAMMA3_EN );
	Ili9341_Spi::writeData(0x00);
	Ili9341_Spi::writeCmd( COLUMN_ADDR );
	Ili9341_Spi::writeData(0x00);
	Ili9341_Spi::writeData(0x00);
	Ili9341_Spi::writeData(0x00);
	Ili9341_Spi::writeData(0xEF);
	Ili9341_Spi::writeCmd( PAGE_ADDR );
	Ili9341_Spi::writeData(0x00);
	Ili9341_Spi::writeData(0x00);
	Ili9341_Spi::writeData(0x01);
	Ili9341_Spi::writeData(0x3F);
	Ili9341_Spi::writeCmd( GAMMA );
	Ili9341_Spi::writeData(0x01);
	Ili9341_Spi::writeCmd( PGAMMA );
	Ili9341_Spi::writeData(0x0F);
	Ili9341_Spi::writeData(0x31);
	Ili9341_Spi::writeData(0x2B);
	Ili9341_Spi::writeData(0x0C);
	Ili9341_Spi::writeData(0x0E);
	Ili9341_Spi::writeData(0x08);
	Ili9341_Spi::writeData(0x4E);
	Ili9341_Spi::writeData(0xF1);
	Ili9341_Spi::writeData(0x37);
	Ili9341_Spi::writeData(0x07);
	Ili9341_Spi::writeData(0x10);
	Ili9341_Spi::writeData(0x03);
	Ili9341_Spi::writeData(0x0E);
	Ili9341_Spi::writeData(0x09);
	Ili9341_Spi::writeData(0x00);
	Ili9341_Spi::writeCmd( NGAMMA );
	Ili9341_Spi::writeData(0x00);
	Ili9341_Spi::writeData(0x0E);
	Ili9341_Spi::writeData(0x14);
	Ili9341_Spi::writeData(0x03);
	Ili9341_Spi::writeData(0x11);
	Ili9341_Spi::writeData(0x07);
	Ili9341_Spi::writeData(0x31);
	Ili9341_Spi::writeData(0xC1);
	Ili9341_Spi::writeData(0x48);
	Ili9341_Spi::writeData(0x08);
	Ili9341_Spi::writeData(0x0F);
	Ili9341_Spi::writeData(0x0C);
	Ili9341_Spi::writeData(0x31);
	Ili9341_Spi::writeData(0x36);
	Ili9341_Spi::writeData(0x0F);

	Ili9341_Spi::writeCmd(0x11); //sleep out
	Ili9341_Spi::delay(200);
	Ili9341_Spi::writeCmd(0x29); // display on
	Ili9341_Spi::delay(100);
	Ili9341_Spi::writeCmd( MAC );
	Ili9341_Spi::writeData( 0x88 ); // default orientation
}

void Ili9341_Spi::setWindow( uint16_t xStart, uint16_t yStart, uint16_t width, uint16_t height )
{
	uint16_t widthTmp, heightTmp;

	widthTmp = xStart + width - 1;
	heightTmp = yStart + height - 1;

	//if(( widthTmp > WIDTH_MAX ) ) { widthTmp = WIDTH_MAX; }
	//if(( heightTmp > HEIGHT_MAX ) ) { heightTmp = HEIGHT_MAX; }

	Ili9341_Spi::writeCmd( COLUMN_ADDR );
	Ili9341_Spi::writeData( xStart >> 8 );
	Ili9341_Spi::writeData( xStart & 0xFF );
	Ili9341_Spi::writeData( widthTmp >> 8 );
	Ili9341_Spi::writeData( widthTmp & 0xFF);
	Ili9341_Spi::writeCmd( PAGE_ADDR );
	Ili9341_Spi::writeData( yStart >> 8 );
	Ili9341_Spi::writeData( yStart & 0xFF);
	Ili9341_Spi::writeData( heightTmp >> 8 );
	Ili9341_Spi::writeData( heightTmp & 0xFF);
	Ili9341_Spi::writeCmd( GRAM );
}

void Ili9341_Spi::sendBuffer( uint32_t n)
{
	spiSetMode16Bit(spiStruct.spi);
	Ili9341_Spi::spiFastMode();

	Ili9341_Spi::dcControl( DATA );
	Ili9341_Spi::csControl( SELECT );

	spiSendBuffer(spiStruct.spi, Graphic::bufferPointerGet(), n);

	Ili9341_Spi::csControl( DESELECT );
	Ili9341_Spi::spiSlowMode();
	spiSetMode8Bit(spiStruct.spi);
}

void Ili9341_Spi::refresh( )
{
	uint32_t pixels = (width*height);

	Ili9341_Spi::setWindow( 0, 0, width, height );
	Ili9341_Spi::sendBuffer( pixels );
}

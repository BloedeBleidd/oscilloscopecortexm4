/*
 * colors.h
 *
 *  Created on: Aug 24, 2016
 *      Author: BloedeBleidd
 */

#ifndef COLORS_H_
#define COLORS_H_

#include "graphic.h"

const Color BLACK			= 0x0000;	/*   0,   0,   0 */
const Color NAVY			= 0x000F;	/*   0,   0, 128 */
const Color DARKGREEN		= 0x03E0;	/*   0, 128,   0 */
const Color DARKCYAN		= 0x03EF;	/*   0, 128, 128 */
const Color MAROON			= 0x7800;	/* 128,   0,   0 */
const Color PURPLE			= 0x780F;	/* 128,   0, 128 */
const Color OLIVE			= 0x7BE0;	/* 128, 128,   0 */
const Color LIGHTGREY		= 0xC618;	/* 192, 192, 192 */
const Color DARKGREY		= 0x7BEF;	/* 128, 128, 128 */
const Color BLUE			= 0x001F;	/*   0,   0, 255 */
const Color GREEN			= 0x07E0;	/*   0, 255,   0 */
const Color CYAN			= 0x07FF;	/*   0, 255, 255 */
const Color RED				= 0xF800;	/* 255,   0,   0 */
const Color MAGENTA			= 0xF81F;	/* 255,   0, 255 */
const Color YELLOW			= 0xFFE0;	/* 255, 255,   0 */
const Color WHITE			= 0xFFFF;	/* 255, 255, 255 */
const Color ORANGE			= 0xFD20;	/* 255, 165,   0 */
const Color GREENYELLOW		= 0xAFE5;	/* 173, 255,  47 */
const Color PINK			= 0xF81F;

#endif /* COLORS_H_ */

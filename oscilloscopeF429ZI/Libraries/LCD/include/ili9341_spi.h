/*
 * Ili9341.h
 *
 *  Created on: 05.07.2017
 *      Author: BloedeBleidd
 */

#ifndef GRAPHICLCD_Ili9341_Ili9341_H_
#define GRAPHICLCD_Ili9341_Ili9341_H_

#include "gpio.h"
#include "spi.h"
#include "FreeRTOS.h"
#include "timers.h"
#include "stm32f4xx.h"
#include "ili9341.h"
#include "graphic.h"

class Ili9341_Spi : public Ili9341, public Graphic
{
public:
	struct SpiInitStruct
	{
		 SPI_TypeDef	*spi;
		 uint8_t		altFunction;
		 GPIO_TypeDef	*gpioChipSelect;
		 PinNumber		chipSelect;
		 GPIO_TypeDef	*gpioDataCommand;
		 PinNumber		dataCommand;
		 GPIO_TypeDef	*gpioMosi;
		 PinNumber		mosi;
		 GPIO_TypeDef	*gpioClock;
		 PinNumber		clock;
	};
protected:
	enum CsControl
	{
		SELECT		= 0,
		DESELECT	= 1,
	};

	enum DcControl
	{
		DATA		= 0,
		COMMAND		= 1,
	};

	SpiInitStruct spiStruct;

	void delay( uint16_t );
	void csControl( CsControl );
	void dcControl( DcControl );
	void spiFastMode();
	void spiSlowMode();
	void writeByte( Ili9341RegisterSize );
	void writeCmd( Ili9341RegisterSize );
	void writeData( Ili9341RegisterSize );
	void setWindow( uint16_t xStart, uint16_t yStart, uint16_t width, uint16_t height );
	void sendBuffer( uint32_t n );
	void hardwareInitialize();
	void Ili9341Initialize();

	Ili9341_Spi(){};
public:
	Ili9341_Spi(uint16_t _width, uint16_t _height, const SpiInitStruct &initStruct, Orientation _orientation = Landscape_2, void *_buffAddress = nullptr);
	~Ili9341_Spi();

	void refresh();
};


#endif /* GRAPHICLCD_Ili9341_Ili9341_H_ */

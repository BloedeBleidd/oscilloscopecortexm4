/*
 * ili9341_ltdc.h
 *
 *  Created on: 16.08.2017
 *      Author: BloedeBleidd
 */

#ifndef LCD_INCLUDE_ILI9341_LTDC_H_
#define LCD_INCLUDE_ILI9341_LTDC_H_

#include "ili9341_spi.h"


class Ili9341_LTDC : public Ili9341_Spi
{
public:
	enum Layer
	{
		BOTTOM	= 0,
		TOP		= 1
	};
private:
	static const uint32_t ILI9341_LCD_WIDTH		= 240;
	static const uint32_t ILI9341_LCD_HEIGHT	= 320;
	static const uint32_t ILI9341_PIXELWIDHT	= 2;

	static const uint32_t ILI9341_HSYNC	= 9;	/* Horizontal synchronization */
	static const uint32_t ILI9341_HBP	= 29;	/* Horizontal back porch      */
	static const uint32_t ILI9341_HFP	= 2;	/* Horizontal front porch     */
	static const uint32_t ILI9341_VSYNC	= 1;	/* Vertical synchronization   */
	static const uint32_t ILI9341_VBP	= 3;	/* Vertical back porch        */
	static const uint32_t ILI9341_VFP	= 2;	/* Vertical front porch       */

	static const uint32_t ACTIVE_W		= 269;
	static const uint32_t ACTIVE_H		= 323;

	static const uint32_t TOTAL_WIDTH	= (ILI9341_HSYNC + ILI9341_HBP + ILI9341_LCD_WIDTH + ILI9341_HFP - 1);
	static const uint32_t TOTAL_HEIGHT	= (ILI9341_VSYNC + ILI9341_VBP + ILI9341_LCD_HEIGHT + ILI9341_VFP - 1 + 2);

	GPIO_TypeDef * GPIOInitTable[23] =
	{
		GPIOA, GPIOA, GPIOA, GPIOA, GPIOA,
		GPIOB, GPIOB, GPIOB, GPIOB, GPIOB, GPIOB,
		GPIOC, GPIOC, GPIOC,
		GPIOD, GPIOD,
		GPIOF,
		GPIOG, GPIOG, GPIOG, GPIOG, GPIOG,
		0
	};

	uint8_t PINInitTable[22] =
	{
		3, 4, 6, 11, 12,
		0, 1, 8, 9, 10, 11,
		6, 7, 10,
		3, 6,
		10,
		6, 7, 10, 11, 12
	};

	uint8_t AFInitTable[22] =
	{
		14, 14, 14, 14, 14,
		9, 9, 14, 14, 14, 14,
		14, 14, 14,
		14, 14,
		14,
		14, 14, 9, 14, 9
	};

	Layer layerCurrent = BOTTOM;
	Color *VRAM1 = nullptr;
	Color *VRAM2 = nullptr;

	bool isLayerTopUsed()			{return VRAM2==nullptr?false:true;};
	void reloadImmediate()			{LTDC->SRCR = LTDC_SRCR_IMR;};
	void reloadVerticalBlanking()	{LTDC->SRCR = LTDC_SRCR_VBR;};
	void hardwareInitialize();
	void Ili9341Initialize();
	void layersInitialize();
	void buffCopy32Bit(void *dst, void const *src, uint32_t len);
	void refresh();
public:
	Ili9341_LTDC(const SpiInitStruct &initStruct, Color *GRAMAddress, void *VRAM1Address, Orientation _orientation = Landscape_2, void *VRAM2Address = nullptr);
	~Ili9341_LTDC();

	void backgroundColorSet(uint32_t color) {LTDC->BCCR = color;};// RGB888 (reserved,red,green,blue)

	bool layerSet(Layer);
	bool layerChange();

	bool layerOpacitySetTop(uint8_t);
	void layerOpacitySetBottom(uint8_t);
	void layerOpacitySetCurrent(uint8_t);
	bool layerOpacitySetNotCurrent(uint8_t);

	bool layerOpacityGetTop(uint8_t &);
	void layerOpacityGetBottom(uint8_t &);
	void layerOpacityGetCurrent(uint8_t &);
	bool layerOpacityGetNotCurrent(uint8_t &);

	bool layerCopyFromTopToBottom();
	bool layerCopyFromBottomToTop();
	bool layerCopyFromCurrent();
	bool layerCopyToCurrent();

	bool layerOn(Layer);
	bool layerOnTop();
	void layerOnBottom();
	void layerOnCurrent();
	bool layerOnNotCurrent();

	bool layerOff(Layer);
	bool layerOffTop();
	void layerOffBottom();
	void layerOffCurrent();
	bool layerOffNotCurrent();

	bool refreshLayer(Layer);
	bool refreshTop();
	void refreshBottom();
	void refreshCurrentLayer();
	bool refreshNotCurrentLayer();

	void displayOn();
	void displayOff();
};



#endif /* LCD_INCLUDE_ILI9341_LTDC_H_ */

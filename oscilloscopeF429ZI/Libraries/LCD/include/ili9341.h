/*
 * ili9341.h
 *
 *  Created on: 17.08.2017
 *      Author: BloedeBleidd
 */

#ifndef LCD_INCLUDE_ILI9341_H_
#define LCD_INCLUDE_ILI9341_H_

#include "colors.h"

class Ili9341
{
protected:
	enum DriverRegisters
	{
		SOFT_RES		= 0x01,	// Software reset
		SLEEP_OUT		= 0x11,	// Sleep out register
		GAMMA			= 0x26,	// Gamma register
		DISPLAY_OFF		= 0x28,	// Display off register
		DISPLAY_ON		= 0x29,	// Display on register
		COLUMN_ADDR		= 0x2A,	// Colomn address register
		PAGE_ADDR		= 0x2B,	// Page address register
		GRAM			= 0x2C,	// GRAM register
		MAC				= 0x36,	// Memory Access Control register
		PIXEL_FORMAT	= 0x3A,	// Pixel Format register
		WDB				= 0x51,	// Write Brightness Display register
		WCD				= 0x53,	// Write Control Display register
		RGB_INTERFACE	= 0xB0,	// RGB Interface Signal Control
		FRAME_CONT_NORM	= 0xB1,	// Frame Control (In Normal Mode)
		FRC				= 0xB2,	// Frame Rate Control register
		BPC				= 0xB5,	// Blanking Porch Control register
		DFC				= 0xB6,	// Display Function Control register
		ENT_MODE_SET	= 0xB7,	// Entry Mode Set
		POWER1			= 0xC0,	// Power Control 1 register
		POWER2			= 0xC1,	// Power Control 2 register
		VCOM1			= 0xC5,	// VCOM Control 1 register
		VCOM2			= 0xC7,	// VCOM Control 2 register
		POWERA			= 0xCB,	// Power control A register
		POWERB			= 0xCF,	// Power control B register
		PGAMMA			= 0xE0,	// Positive Gamma Correction register
		NGAMMA			= 0xE1,	// Negative Gamma Correction register
		DTCA			= 0xE8,	// Driver timing control A
		DTCB			= 0xEA,	// Driver timing control B
		POWER_SEQ		= 0xED,	// Power on sequence register
		GAMMA3_EN		= 0xF2,	// 3 Gamma enable register
		INTERFACE		= 0xF6,	// Interface control register
		PRC				= 0xF7	// Pump ratio control register
	};

	typedef uint8_t Ili9341RegisterSize;

	static const uint16_t HEIGHT_MAX = 320;
	static const uint16_t WIDTH_MAX = 240;
};


#endif /* LCD_INCLUDE_ILI9341_H_ */

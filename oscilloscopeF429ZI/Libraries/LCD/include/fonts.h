/*
 * fonts.h
 *
 *  Created on: Aug 21, 2016
 *      Author: BloedeBleidd
 */

#ifndef FONTS_H_
#define FONTS_H_

#include <stdint.h>

/*
 * Font struct
 */
typedef struct
{
	uint8_t FontWidth;
	uint8_t FontHeight;
	const uint16_t *data;
} FontDef_t;


#endif /* FONTS_H_ */

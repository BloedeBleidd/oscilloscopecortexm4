/*
 * graphic.h
 *
 *  Created on: 05.07.2017
 *      Author: BloedeBleidd
 */

#ifndef LCD_GRAPHIC_GRAPHIC_H_
#define LCD_GRAPHIC_GRAPHIC_H_

#include <string>
#include "fonts.h"

extern FontDef_t Font_7x10;
extern FontDef_t Font_11x18;
extern FontDef_t Font_16x26;

typedef uint16_t Color;

class Graphic
{
public:
	enum Orientation
	{
		Portrait_1,
		Portrait_2,
		Landscape_1,
		Landscape_2
	};

	enum StringToIntBase
	{
		DECIMAL = 10,
		HEXADECIMAL = 16,
		BINARY = 2
	};

	enum Corner
	{
		FIRST		= 1,
		SECOND		= 2,
		THIRD		= 4,
		FOURTH		= 8
	};

private:
	int32_t xCursor = 0, yCursor = 0;
	Color *graphicBuffer;
	StringToIntBase integerBase = DECIMAL;
	uint8_t floatPrecision = 10;

	void ftoa(float f, char *str, uint8_t precision);
protected:
	Orientation orientation;
	uint16_t width, height;
	uint32_t pixelAmount;
	FontDef_t font;

	void bufferPointerSet(Color* buffer)	{graphicBuffer = buffer;};
	void createBuffer(void *address, Color *&buffer);
	void deleteBuffer(Color *buffer);
public:
	void drawPixel( int32_t x, int32_t y, Color color );

	void drawLine			( int32_t xBeg, int32_t yBeg, int32_t xEnd, int32_t yEnd, Color color );
	void drawLineVertical	( int32_t x, int32_t yBeg, int32_t yEnd, Color color );
	void drawLineHorizontal	( int32_t xBeg, int32_t xEnd, int32_t y, Color color );

	void drawRectangle							( int32_t xStart, int32_t yStart, int32_t xEnd, int32_t yEnd, Color color );
	void drawRectangleAndFill					( int32_t xStart, int32_t yStart, int32_t xEnd, int32_t yEnd, Color color );
	void drawRectangleWithRoundedEdges			( int32_t xStart, int32_t yStart, int32_t xEnd, int32_t yEnd, uint16_t r, Color color );
	void drawRectangleWithRoundedEdgesAndFill	( int32_t xStart, int32_t yStart, int32_t xEnd, int32_t yEnd, uint16_t r, Color color );

	void drawCircle					( int32_t x, int32_t y, uint16_t r, Color color );
	void drawCircleAndFill			( int32_t x, int32_t y, uint16_t r, Color color );
	void drawCircleCorner			( int32_t x, int32_t y, uint16_t r, Corner corner, Color color );
	void drawCircleCornerAndFill	( int32_t x, int32_t y, uint16_t r, Corner corner, Color color );

	void drawTriangle			(int32_t xA, int32_t yA, int32_t xB, int32_t yB, int32_t xC, int32_t yC, Color color);
	void drawTriangleAndFill	(int32_t xA, int32_t yA, int32_t xB, int32_t yB, int32_t xC, int32_t yC, Color color);

	void drawChar		(int32_t x, int32_t y, const char ch, Color color );
	void drawString		(int32_t x, int32_t y, const char *str, Color color );
	void drawString		(int32_t x, int32_t y, const std::string &str, Color color );
	void drawInteger	(int32_t x, int32_t y, int32_t data, Color color );
	void drawFloat		(int32_t x, int32_t y, float data, Color color );

	void drawData	(int32_t x, int32_t y, const char ch, Color color );
	void drawData	(int32_t x, int32_t y, const char *str, Color color );
	void drawData	(int32_t x, int32_t y, const std::string &str, Color color );
	void drawData	(int32_t x, int32_t y, int32_t data, Color color );
	void drawData	(int32_t x, int32_t y, float data, Color color );

	void drawBitmap16Bit(int32_t x, int32_t y, const Color *, uint16_t _width, uint16_t _height);

	void fill( Color color );

	void fontSet(FontDef_t _font)			{font = _font;};
	FontDef_t fontGet()						{return font;};
	uint8_t fontGetWidth()					{return font.FontWidth;};
	uint8_t fontGetHeight()					{return font.FontHeight;};

	int32_t cursorGetX()					{return xCursor;};
	int32_t cursorGetY()					{return yCursor;};

	void orientationSet( Orientation o)		{orientation = o;};
	Orientation orientationGet()			{return orientation;};

	void precisionSet(uint8_t p)			{floatPrecision = p;};
	uint8_t precisionGet()					{return floatPrecision;};

	void baseStringSet(StringToIntBase b)	{integerBase = b;};
	StringToIntBase baseStringGet()			{return integerBase;};

	uint16_t getWidth()						{return width;}
	uint16_t getHeight()					{return height;}

	Color*& bufferPointerGet()				{return graphicBuffer;};
};

#endif /* LCD_GRAPHIC_GRAPHIC_H_ */

/*
 * RTOS_includes.h
 *
 *  Created on: 29.07.2017
 *      Author: BloedeBleidd
 */

#ifndef INCLUDE_RTOS_INCLUDES_H_
#define INCLUDE_RTOS_INCLUDES_H_

/****************************** Config RTOS includes ******************************/
#include "FreeRTOSConfig.h"
/****************************** Kernel includes ******************************/
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"
#include "queue.h"
#include "priorities.h"
#include "StackMacros.h"

#endif /* INCLUDE_RTOS_INCLUDES_H_ */

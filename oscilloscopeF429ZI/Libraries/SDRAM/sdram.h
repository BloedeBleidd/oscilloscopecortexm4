//=================================================================================================
// STM32F429I-Discovery SDRAM configuration
// Author : Radoslaw Kwiecien
// e-mail : radek@dxp.pl
// http://en.radzio.dxp.pl/stm32f429idiscovery/
// Date : 24.11.2013
//=================================================================================================
#ifndef SDRAM_H_
#define SDRAM_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "stm32f4xx.h"

#define SDRAM_BASE	(uint32_t)(0xD0000000)
#define SDRAM_END	(uint32_t)(0xD0800000)

#define SDRAM_GetSize()                  	(SDRAM_SIZE)
#define SDRAM_Write8(address, value)		(*(__IO uint8_t *) (SDRAM_BASE + (address)) = (value))
#define SDRAM_Read8(address)				(*(__IO uint8_t *) (SDRAM_BASE + (address)))
#define SDRAM_Write16(address, value)		(*(__IO uint16_t *) (SDRAM_BASE + (address)) = (value))
#define SDRAM_Read16(address)				(*(__IO uint16_t *) (SDRAM_BASE + (address)))
#define SDRAM_Write32(address, value)		(*(__IO uint32_t *) (SDRAM_BASE + (address)) = (value))
#define SDRAM_Read32(address)				(*(__IO uint32_t *) (SDRAM_BASE + (address)))
#define SDRAM_WriteFloat(address, value)	(*(__IO float *) (SDRAM_BASE + (address)) = (value))
#define SDRAM_ReadFloat(address)			(*(__IO float *) (SDRAM_BASE + (address)))

void SDRAM_Init(void);

#ifdef __cplusplus
}
#endif

#endif /* SDRAM_H_ */
//=================================================================================================
// End of file
//=================================================================================================

/*
 * stmpe811.cpp
 *
 *  Created on: Sep 2, 2016
 *      Author: BloedeBleidd
 */

#include "stmpe811.h"

// STMPE811 Chip ID on reset
const uint16_t STMPE811_CHIP_ID_VALUE =	0x0811;	//Chip ID
// I2C address
const uint8_t STMPE811_ADDRESS =		0x82;
// Registers
const uint8_t STMPE811_CHIP_ID =		0x00;	//STMPE811 Device identification
const uint8_t STMPE811_ID_VER =			0x02;	//STMPE811 Revision number; 0x01 for engineering sample; 0x03 for final silicon
const uint8_t STMPE811_SYS_CTRL1 =		0x03;	//Reset control
const uint8_t STMPE811_SYS_CTRL2 =		0x04;	//Clock control
const uint8_t STMPE811_SPI_CFG =		0x08;	//SPI interface configuration
const uint8_t STMPE811_INT_CTRL =		0x09;	//Interrupt control register
const uint8_t STMPE811_INT_EN =			0x0A;	//Interrupt enable register
const uint8_t STMPE811_INT_STA =		0x0B;	//Interrupt status register
const uint8_t STMPE811_GPIO_EN =		0x0C;	//GPIO interrupt enable register
const uint8_t STMPE811_GPIO_INT_STA =	0x0D;	//GPIO interrupt status register
const uint8_t STMPE811_ADC_INT_EN =		0x0E;	//ADC interrupt enable register
const uint8_t STMPE811_ADC_INT_STA =	0x0F;	//ADC interface status register
const uint8_t STMPE811_GPIO_SET_PIN =	0x10;	//GPIO set pin register
const uint8_t STMPE811_GPIO_CLR_PIN =	0x11;	//GPIO clear pin register
const uint8_t STMPE811_MP_STA =			0x12;	//GPIO monitor pin state register
const uint8_t STMPE811_GPIO_DIR =		0x13;	//GPIO direction register
const uint8_t STMPE811_GPIO_ED =		0x14;	//GPIO edge detect register
const uint8_t STMPE811_GPIO_RE =		0x15;	//GPIO rising edge register
const uint8_t STMPE811_GPIO_FE =		0x16;	//GPIO falling edge register
const uint8_t STMPE811_GPIO_AF =		0x17;	//alternate function register
const uint8_t STMPE811_ADC_CTRL1 =		0x20;	//ADC control
const uint8_t STMPE811_ADC_CTRL2 =		0x21;	//ADC control
const uint8_t STMPE811_ADC_CAPT =		0x22;	//To initiate ADC data acquisition
const uint8_t STMPE811_ADC_DATA_CHO =	0x30;	//ADC channel 0
const uint8_t STMPE811_ADC_DATA_CH1 =	0x32;	//ADC channel 1
const uint8_t STMPE811_ADC_DATA_CH2 =	0x34;	//ADC channel 2
const uint8_t STMPE811_ADC_DATA_CH3 =	0x36;	//ADC channel 3
const uint8_t STMPE811_ADC_DATA_CH4 =	0x38;	//ADC channel 4
const uint8_t STMPE811_ADC_DATA_CH5 =	0x3A;	//ADC channel 5
const uint8_t STMPE811_ADC_DATA_CH6 =	0x3C;	//ADC channel 6
const uint8_t STMPE811_ADC_DATA_CH7 =	0x3E;	//ADC channel 7
const uint8_t STMPE811_TSC_CTRL =		0x40;	//4-wire touchscreen controller setup
const uint8_t STMPE811_TSC_CFG =		0x41;	//Touchscreen controller configuration
const uint8_t STMPE811_WDW_TR_X =		0x42;	//Window setup for top right X
const uint8_t STMPE811_WDW_TR_Y =		0x44;	//Window setup for top right Y
const uint8_t STMPE811_WDW_BL_X =		0x46;	//Window setup for bottom left X
const uint8_t STMPE811_WDW_BL_Y =		0x48;	//Window setup for bottom left Y
const uint8_t STMPE811_FIFO_TH =		0x4A;	//FIFO level to generate interrupt
const uint8_t STMPE811_FIFO_STA =		0x4B;	//Current status of FIFO
const uint8_t STMPE811_FIFO_SIZE =		0x4C;	//Current filled level of FIFO
const uint8_t STMPE811_TSC_DATA_X =		0x4D;	//Data port for touchscreen controller data access
const uint8_t STMPE811_TSC_DATA_Y =		0x4F;	//Data port for touchscreen controller data access
const uint8_t STMPE811_TSC_DATA_Z =		0x51;	//Data port for touchscreen controller data access
const uint8_t STMPE811_TSC_DATA_XYZ =	0x52;	//Data port for touchscreen controller data access
const uint8_t STMPE811_TSC_FRACTION_Z =	0x56;	//Touchscreen controller FRACTION_Z
const uint8_t STMPE811_TSC_DATA =		0x57;	//Data port for touchscreen controller data access
const uint8_t STMPE811_TSC_I_DRIVE =	0x58;	//Touchscreen controller drivel
const uint8_t STMPE811_TSC_SHIELD =		0x59;	//Touchscreen controller shield
const uint8_t STMPE811_TEMP_CTRL =		0x60;	//Temperature sensor setup
const uint8_t STMPE811_TEMP_DATA =		0x61;	//Temperature data access port
const uint8_t STMPE811_TEMP_TH =		0x62;	//Threshold for temperature controlled interrupt


Stmpe811::Stmpe811( uint16_t _width, uint16_t _height, Orientation orient, const I2CInitStruct &initStruct )
{
	width = _width;
	height = _height;
	orientation = orient;
	i2cStruct = initStruct;
	x = 0;
	y = 0;
	z = 0;
	zMin = 0;
	pressState = false;
	calibrate(0, width, 0, height);

	Stmpe811::hardwareInitialize();
	Stmpe811::driverInitialize();
}

void Stmpe811::delayMs(uint16_t del)
{
	while(del--)
	{
		vTaskDelay(pdMS_TO_TICKS( 1 ));
	}
}

void Stmpe811::resetI2c (void)
{
	i2cStruct.i2c->CR1 |= I2C_CR1_SWRST;
	i2cStruct.i2c->CR1 &= ~I2C_CR1_SWRST;
	i2cStruct.i2c->CR1 &= ~I2C_CR1_PE;
	while(i2cStruct.i2c->CR1 & I2C_CR1_PE);
}

void Stmpe811::setI2cTimings (void)
{
	i2cStruct.i2c->CR2 = 45;			//frequency
	i2cStruct.i2c->TRISE = 37;			// limit slope
	i2cStruct.i2c->CCR = 178;			// setup speed (100kHz)
	i2cStruct.i2c->CR1 = I2C_CR1_PE;	// enable
}

void Stmpe811::writeRegister(Data addressRegister, Data data)
{
	Stmpe811::requestForStart();			// request a start
	Stmpe811::waitForStartFlag();			// wait for start to finish // read of SR1 clears the flag
	Stmpe811::sendI2cData(STMPE811_ADDRESS);// transfer address (Transmit)
	Stmpe811::waitForAdressFlag();			// wait for address transfer
	Stmpe811::clearFlag();					// clear the flag
	Stmpe811::waitForTXEFlag();				// wait for DR empty
	Stmpe811::sendI2cData(addressRegister);
	Stmpe811::waitForTXEFlag();				// wait for DR empty
	Stmpe811::sendI2cData(data);			// transfer one byte
	Stmpe811::waitForBusBusy();				// wait for bus not-busy
	Stmpe811::requestForStop();				// request a stop
}

void Stmpe811::readRegisters(Data addressRegister, Data* data, Length length)
{
	Stmpe811::requestForAck();
	Stmpe811::requestForStart();					// request a start
	Stmpe811::waitForStartFlag();					// wait for start to finish // read of SR1 clears the flag
	Stmpe811::sendI2cData(STMPE811_ADDRESS);		// transfer address (Transmit)
	Stmpe811::waitForAdressFlag();					// wait for address transfer
	Stmpe811::clearFlag();							// clear the flag
	Stmpe811::waitForTXEFlag();						// wait for DR empty
	Stmpe811::sendI2cData(addressRegister);
	Stmpe811::waitForBusyFlag();					// wait for bus not-busy
	Stmpe811::requestForStart();					// request a start
	Stmpe811::waitForStartFlag();					// wait for start to finish // read of SR1 clears the flag
	Stmpe811::sendI2cData(STMPE811_ADDRESS|0x01);	// transfer address (Receive)
	Stmpe811::waitForAdressFlag();					// wait for address transfer
	Stmpe811::clearFlag();							// clear the flag

	while(length)									// transfer whole block
	{
		if(length == 1)	Stmpe811::resetAck();
		Stmpe811::waitForRXEFlag();					// wait for DR fill
		*( data++ ) = Stmpe811::receiveI2cData();	// receive one byte, increment pointer
		length--;
	}

	Stmpe811::requestForStop();				// request a stop
}

void Stmpe811::hardwareInitialize (void)
{
	gpioPinConfiguration(i2cStruct.gpioSCL, i2cStruct.SCL, GPIO_MODE_ALTERNATE_OPEN_DRAIN_HIGH_SPEED_PULL_UP);
	gpioPinAlternateFunctionConfiguration( i2cStruct.gpioSCL, i2cStruct.SCL, i2cStruct.alterfunction );
	gpioPinConfiguration(i2cStruct.gpioSDA, i2cStruct.SDA, GPIO_MODE_ALTERNATE_OPEN_DRAIN_HIGH_SPEED_PULL_UP);
	gpioPinAlternateFunctionConfiguration( i2cStruct.gpioSDA, i2cStruct.SDA, i2cStruct.alterfunction );

	Stmpe811::resetI2c();		// Software reset
	Stmpe811::setI2cTimings();	// i2c timings setup
}


Stmpe811::Result Stmpe811::driverInitialize()
{
	Data tmp;
	uint8_t Buffer[2];
	int16_t chipID;

	/* Reset */
	Stmpe811::writeRegister(STMPE811_SYS_CTRL1, 0x02);		// software reset start
	Stmpe811::delayMs(5);
	Stmpe811::writeRegister(STMPE811_SYS_CTRL1, 0x00);		// software reset stop
	Stmpe811::delayMs(2);

	/* Check for driver connected */
	Stmpe811::readRegisters(STMPE811_CHIP_ID,Buffer,2);
	chipID = Buffer[0]<<8|Buffer[1];
	if (chipID != STMPE811_CHIP_ID_VALUE)
		return ERROR;

	/* Reset */
	Stmpe811::writeRegister(STMPE811_SYS_CTRL1, 0x02);		// software reset start
	Stmpe811::delayMs(5);
	Stmpe811::writeRegister(STMPE811_SYS_CTRL1, 0x00);		// software reset stop
	Stmpe811::delayMs(2);

	/* Get the current register value */
	Stmpe811::readRegisters(STMPE811_SYS_CTRL2, &tmp, 1);
	tmp &= ~(0x01);
	Stmpe811::writeRegister(STMPE811_SYS_CTRL2, tmp);
	Stmpe811::readRegisters(STMPE811_SYS_CTRL2, &tmp, 1);
	tmp &= ~(0x02);
	Stmpe811::writeRegister(STMPE811_SYS_CTRL2, tmp);

	/* Select Sample Time, bit number and ADC Reference */
	Stmpe811::writeRegister(STMPE811_ADC_CTRL1, 0x49);
	Stmpe811::delayMs(2);

	/* Select the ADC clock speed: 3.25 MHz */
	Stmpe811::writeRegister(STMPE811_ADC_CTRL2, 0x01);

	/* Select TSC pins in non default mode */
	Stmpe811::readRegisters(STMPE811_GPIO_AF, &tmp, 1);
	tmp |= 0x1E;
	Stmpe811::writeRegister(STMPE811_GPIO_AF, tmp);

	/* Select 2 nF filter capacitor */
	/* Configuration:
	- Touch average control    : 4 samples
	- Touch delay time         : 500 uS
	- Panel driver setting time: 500 uS
	*/
	Stmpe811::writeRegister(STMPE811_TSC_CFG, 0x9A);

	/* Configure the Touch FIFO threshold: single point reading */
	Stmpe811::writeRegister(STMPE811_FIFO_TH, 0x01);

	/* Clear the FIFO memory content. */
	Stmpe811::writeRegister(STMPE811_FIFO_STA, 0x01);

	/* Put the FIFO back into operation mode  */
	Stmpe811::writeRegister(STMPE811_FIFO_STA, 0x00);

	/* Set the range and accuracy pf the pressure measurement (Z) :
	- Fractional part :7
	- Whole part      :1
	*/
	Stmpe811::writeRegister(STMPE811_TSC_FRACTION_Z, 0x0F);

	/* Set the driving capability (limit) of the device for TSC pins: 50mA */
	Stmpe811::writeRegister(STMPE811_TSC_I_DRIVE, 0x01);

	/* Touch screen control configuration (enable TSC):
	- No window tracking index
	- XYZ acquisition mode
	*/
	Stmpe811::writeRegister(STMPE811_TSC_CTRL, 0x01);

	/* Clear all the status pending bits if any */
	Stmpe811::writeRegister(STMPE811_INT_STA, 0xFF);
	Stmpe811::delayMs(2);

	/* Check for driver connected */
	Stmpe811::readRegisters(STMPE811_CHIP_ID,Buffer,2);
	chipID = Buffer[0]<<8|Buffer[1];
	if (chipID != STMPE811_CHIP_ID_VALUE)
		return ERROR;
	return OK;
}

/*
 * Stmpe811::writeRegister(STMPE811_SYS_CTRL1, 0x02);		// software reset start
	Stmpe811::delayMs(5);
	Stmpe811::writeRegister(STMPE811_SYS_CTRL1, 0x00);		// software reset stop
	Stmpe811::delayMs(2);
	Stmpe811::writeRegister(STMPE811_SYS_CTRL2, 0x04);		// gpio off
	Stmpe811::writeRegister(STMPE811_INT_EN, 0x05);			// interrupts on
	Stmpe811::writeRegister(STMPE811_ADC_CTRL1, 0x49);		// sample time = 80, resolution = 12bit
	Stmpe811::delayMs(5);
	Stmpe811::writeRegister(STMPE811_ADC_CTRL2, 0x01);		// adc clk = 3,25Mhz
	Stmpe811::writeRegister(STMPE811_GPIO_AF, 0x00);		// gpio adc mode
	Stmpe811::writeRegister(STMPE811_TSC_CFG, 0xA3);		// settling 1ms, touch detect delay = 1ms, average control = 4
	Stmpe811::writeRegister(STMPE811_FIFO_TH, 0x01);		// fifo threshold  = 1
	Stmpe811::writeRegister(STMPE811_FIFO_STA, 0x01);		// reset fifo start
	Stmpe811::writeRegister(STMPE811_FIFO_STA, 0x00);		// reset fifo stop
	Stmpe811::writeRegister(STMPE811_TSC_FRACTION_Z, 0x07);	// fractional = 7, whole part = 1
	Stmpe811::writeRegister(STMPE811_TSC_I_DRIVE, 0x01);	// 50mA
	Stmpe811::writeRegister(STMPE811_TSC_CTRL, 0x01);		// enable, use (x,y,z)
	Stmpe811::writeRegister(STMPE811_INT_STA, 0xFF);		// reset interrupts
	Stmpe811::writeRegister(STMPE811_INT_CTRL, 0x03);		// on global interrupts, high level interrupts
	Stmpe811::readRegisters(STMPE811_CHIP_ID,Buffer,2);

	chipID = Buffer[0]<<8|Buffer[1];
	if (chipID != STMPE811_CHIP_ID_VALUE)
		return ERROR;
	return OK;
 */

/*
bool Stmpe811::update(void)
{
	uint8_t cRegVal;

	Stmpe811::readRegisters(STMPE811_INT_STA, &cRegVal, 1);

	if (cRegVal&0x02)
	{
		uint8_t buffer[4];

		Stmpe811::readRegisters(STMPE811_TSC_DATA_XYZ, buffer, 4);
		z = buffer[3];
		if(Stmpe811::isEnoughPressed())
		{
			y = (buffer[0] << 4) | (buffer[1] >> 4);
			x = ((buffer[1] & 0x0F) << 8) | buffer[2];
			Stmpe811::convertDriverDataToXY();
			Stmpe811::calculateRealCoordinates();
			TouchPanel::considerOrientation();
		}
	}

	if (cRegVal&0x01)
	{
		if(state == Pressed)
			state = Released;
		else
			state = Pressed;
		result = true;
	}

	if ((cRegVal&0x03)==3)	Stmpe811::writeRegister(STMPE811_INT_STA, 0x03);
	else if(cRegVal&0x01)	Stmpe811::writeRegister(STMPE811_INT_STA, 0x01);
	else if(cRegVal&0x02)	Stmpe811::writeRegister(STMPE811_INT_STA, 0x02);

	return TouchPanel::isPressed();
}
*/

void Stmpe811::convertDriverDataToXY()
{
	x = ((uint32_t)(x * width) / STMPE911_MAX_RES);
	y = ((uint32_t)(y * height) / STMPE911_MAX_RES);
}

void Stmpe811::resetFIFO()
{
	Stmpe811::writeRegister(STMPE811_FIFO_STA, 0x01);
	Stmpe811::writeRegister(STMPE811_FIFO_STA, 0x00);
}

bool Stmpe811::update()
{
	uint8_t val;

	/* Read */
	Stmpe811::readRegisters(STMPE811_TSC_CTRL, &val, 1);
	if ((val & 0x80) != 0)
	{
		/* Clear all the status pending bits if any */
		//Stmpe811::writeRegister(STMPE811_INT_STA, 0xFF);

		//Pressed
		uint8_t buffer[4];
		Stmpe811::readRegisters(STMPE811_TSC_DATA_XYZ, buffer, 4);

		//Reset Fifo
		Stmpe811::resetFIFO();

		//Check for valid data
		z = buffer[3];
		if(Stmpe811::isEnoughPressed())
		{
			y = (buffer[0] << 4) | (buffer[1] >> 4);
			x = ((buffer[1] & 0x0F) << 8) | buffer[2];
			Stmpe811::convertDriverDataToXY();
			pressState = TouchPanel::calculateRealCoordinates();
		}
	}
	else
	{
		//Not pressed
		pressState = false;

		//Reset Fifo
		Stmpe811::resetFIFO();
	}

	return pressState;
}

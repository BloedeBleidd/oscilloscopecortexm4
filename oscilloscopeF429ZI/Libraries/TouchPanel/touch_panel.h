/*
 * touch_panel.h
 *
 *  Created on: 01.08.2017
 *      Author: BloedeBleidd
 */

#ifndef TOUCHPANEL_TOUCH_PANEL_H_
#define TOUCHPANEL_TOUCH_PANEL_H_

#include <cstdint>

class TouchPanel
{
public:
	enum Orientation
	{
		Portrait_1,
		Portrait_2,
		Landscape_1,
		Landscape_2
	};
protected:
	uint16_t width, height, x, y, xMin, xMax, yMin, yMax;
	uint8_t z, zMin;
	Orientation orientation;
	bool pressState;

	void considerOrientation();
	bool isEnoughPressed();
	bool calculateRealCoordinates();

public:
	enum Coordinates { X, Y, Z };

	void calibrate(uint16_t x_min, uint16_t x_max, uint16_t y_min, uint16_t y_max);
	void setMinPressureForce(uint16_t force) {zMin = force;};
	void resize(uint16_t _width, uint16_t _height, Orientation orient);
	void resize(uint16_t _width, uint16_t _height);

	uint16_t getX() const;
	uint16_t getY() const;
	uint16_t getZ() const {return z;};
	uint16_t getCoordinate(Coordinates xyz) const;

	bool getState() const {return pressState;};
	void setState(bool stat) {pressState = stat;};
	bool isPressed() const {return pressState;};
	bool isReleased() const {return !pressState;};
};


#endif /* TOUCHPANEL_TOUCH_PANEL_H_ */

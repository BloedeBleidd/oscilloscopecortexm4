/*
 * stmpe811.h
 *
 *  Created on: Sep 2, 2016
 *      Author: BloedeBleidd
 */

#ifndef STMPE811_H_
#define STMPE811_H_

#include "../../Libraries/BloedeBleidd_Libraries/include/gpio.h"
#include "../../Libraries/RTOS/include/FreeRTOS.h"
#include "../../Libraries/RTOS/include/timers.h"
#include "stm32f4xx.h"
#include "touch_panel.h"

/*
STMPE811	STM32F4xx	Description
SCL			PA8			Clock pin for I2C
SDA			PC9			Data pin for I2C
INT		    PA15		Int pin for STMPE811
I2C3
AF4
*/

class Stmpe811 : public TouchPanel
{
public: // public enumerations and typedefs
	typedef struct
	{
		I2C_TypeDef		*i2c;
		uint8_t			alterfunction;
		GPIO_TypeDef	*gpioSCL;
		PinNumber		SCL;
		GPIO_TypeDef	*gpioSDA;
		PinNumber		SDA;
	} I2CInitStruct;

private: // private variables
	typedef uint8_t Data;
	typedef uint8_t Length;

	enum Result
	{
		OK,       	// Result OK. Used on initialization
		ERROR    	// Result error. Used on initialization
	};

	static const uint16_t STMPE911_MAX_RES = 4096;
	uint32_t dummy;
	I2CInitStruct i2cStruct;

	void delayMs(uint16_t del);
	void hardwareInitialize();
	Result driverInitialize();
	void resetI2c();
	void setI2cTimings();
	void resetAck() 			{i2cStruct.i2c->CR1 &= ~I2C_CR1_ACK;};
	void requestForStart() 		{i2cStruct.i2c->CR1 |= I2C_CR1_START;};
	void requestForStop() 		{i2cStruct.i2c->CR1 |= I2C_CR1_STOP;};
	void requestForAck() 		{i2cStruct.i2c->CR1 |= I2C_CR1_ACK;};
	void sendI2cData(Data data) {i2cStruct.i2c->DR = data;};
	Data receiveI2cData() 		{return i2cStruct.i2c->DR;};
	void clearFlag() 			{dummy = i2cStruct.i2c->SR2;};
	void waitForStartFlag() 	{while(!(i2cStruct.i2c->SR1 & I2C_SR1_SB )) {}};
	void waitForAdressFlag()	{while(!(i2cStruct.i2c->SR1 & I2C_SR1_ADDR)) {}};
	void waitForTXEFlag()		{while(!(i2cStruct.i2c->SR1 & I2C_SR1_TXE)) {}};
	void waitForRXEFlag()		{while(!(i2cStruct.i2c->SR1 & I2C_SR1_RXNE)) {}};
	void waitForBusyFlag()		{while(!(i2cStruct.i2c->SR1 & I2C_SR1_BTF)) {}};
	void waitForBusBusy()		{while((!(i2cStruct.i2c->SR1 & I2C_SR1_TXE)) || (!(i2cStruct.i2c->SR1 & I2C_SR1_BTF))) {}};
	void writeRegister(Data addressRegister, Data data);
	void readRegisters(Data addressRegister, Data* data, Length length);

	void convertDriverDataToXY();
	void resetFIFO();

public: // public interface
	Stmpe811( uint16_t _width, uint16_t _height, Orientation orient, const I2CInitStruct &initStruct );
	~Stmpe811();

	bool update(void);
};


#endif /* STMPE811_H_ */

/*
 * touch_panel.cpp
 *
 *  Created on: 01.08.2017
 *      Author: BloedeBleidd
 */

#include "touch_panel.h"

void TouchPanel::calibrate(uint16_t x_min, uint16_t x_max, uint16_t y_min, uint16_t y_max)
{
	xMin = x_min;
	xMax = x_max;
	yMin = y_min;
	yMax = y_max;
}

uint16_t TouchPanel::getX() const
{
	switch(orientation)
	{
	case Portrait_2:
		return height-y;

	case Portrait_1:
		return y;

	case Landscape_2:
		return width-x;

	case Landscape_1:
		return x;
	}

	return 0;
}

uint16_t TouchPanel::getY() const
{
	switch(orientation)
	{
	case Portrait_2:
		return x;

	case Portrait_1:
		return width-x;

	case Landscape_2:
		return height-y;

	case Landscape_1:
		return y;
	}

	return 0;
}

uint16_t TouchPanel::getCoordinate(Coordinates xyz) const
{
	if(X == xyz)
		return TouchPanel::getX();
	else if(Y == xyz)
		return TouchPanel::getY();
	else
		return TouchPanel::getZ();;
}

void TouchPanel::resize(uint16_t _width, uint16_t _height, Orientation orient)
{
	width = _width;
	height = _height;
	orientation = orient;
}

void TouchPanel::resize(uint16_t _width, uint16_t _height)
{
	width = _width;
	height = _height;
}

bool TouchPanel::isEnoughPressed()
{
	if(z > zMin)
		return true;
	return false;
}

bool TouchPanel::calculateRealCoordinates()
{
	if(!(x < width && x > xMin && y < height && y > yMin))
		return false;

	if(x < width && x > xMin)
		x = (uint16_t)( (uint32_t)( (x - xMin) * width ) / (xMax - xMin) );

	if(y < height && y > yMin)
		y = (uint16_t)( (uint32_t)( (y - yMin) * height ) / (yMax - yMin) );

	return true;
}

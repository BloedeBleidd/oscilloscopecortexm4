/*
 * spi.h
 *
 *  Created on: 16.08.2017
 *      Author: BloedeBleidd
 */

#ifndef BLOEDEBLEIDD_LIBRARIES_INCLUDE_SPI_H_
#define BLOEDEBLEIDD_LIBRARIES_INCLUDE_SPI_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "bloedebleidd_libraries.h"

typedef enum
{
	SPI_OK,
	SPI_ERROR,
}SpiResult;

typedef enum
{
	SPI_PRESCALER_2		= (0x00),
	SPI_PRESCALER_4		= (SPI_CR1_BR_0),
	SPI_PRESCALER_8		= (SPI_CR1_BR_1),
	SPI_PRESCALER_16	= (SPI_CR1_BR_0 | SPI_CR1_BR_1),
	SPI_PRESCALER_32	= (SPI_CR1_BR_2),
	SPI_PRESCALER_64	= (SPI_CR1_BR_0 | SPI_CR1_BR_2),
	SPI_PRESCALER_128	= (SPI_CR1_BR_1 | SPI_CR1_BR_2),
	SPI_PRESCALER_256	= (SPI_CR1_BR),
}SpiPrescaler;

typedef enum
{
	SPI_MASTER_
}SpiMode;



void spiInitialize(SPI_TypeDef *spi, SpiMode mode, SpiPrescaler presc);
void spiWaitUntilSpiTransmits(SPI_TypeDef *spi);
void spiWwaitUntilSpiReceive(SPI_TypeDef *spi);
void spiWwaitUntilSpiIsBusy(SPI_TypeDef *spi);
void spiSetMode8Bit(SPI_TypeDef *spi);
void spiSetMode16Bit(SPI_TypeDef *spi);
void spiSetPrescaler(SPI_TypeDef *spi, SpiPrescaler presc);
void spiEnable(SPI_TypeDef *spi);
void spiDisable(SPI_TypeDef *spi);
uint16_t spiExchangeData(SPI_TypeDef *spi, uint16_t data);
void spiSendBuffer(SPI_TypeDef *spi, uint16_t *data, uint32_t n);
void spiReceiveBuffer(SPI_TypeDef *spi, uint16_t *data, uint32_t n);
void spiExchangeBuffers(SPI_TypeDef *spi, uint16_t *dataSend, uint16_t *dataReceive, int32_t n);

#ifdef __cplusplus
}
#endif

#endif /* BLOEDEBLEIDD_LIBRARIES_INCLUDE_SPI_H_ */

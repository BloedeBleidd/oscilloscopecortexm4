/*
 * spi.c
 *
 *  Created on: 16.08.2017
 *      Author: BloedeBleidd
 */

#include "spi.h"

void spiInitialize(SPI_TypeDef *spi, SpiMode mode, SpiPrescaler presc)
{

}

void spiWaitUntilSpiTransmits(SPI_TypeDef *spi)
{
	while(!(spi->SR & SPI_SR_TXE))	{};
}

void spiWwaitUntilSpiReceive(SPI_TypeDef *spi)
{
	while(!(spi->SR & SPI_SR_RXNE))	{};
}

void spiWwaitUntilSpiIsBusy(SPI_TypeDef *spi)
{
	while(spi->SR & SPI_SR_BSY)		{};
}

void spiSetMode8Bit(SPI_TypeDef *spi)
{
	spiDisable(spi);
	spi->CR1 &= ~SPI_CR1_DFF;
	spiEnable(spi);
}

void spiSetMode16Bit(SPI_TypeDef *spi)
{
	spiDisable(spi);
	spi->CR1 |= SPI_CR1_DFF;
	spiEnable(spi);
}

void spiSetPrescaler(SPI_TypeDef *spi, SpiPrescaler presc)
{
	spiDisable(spi);
	spi->CR1 = (spi->CR1 & ~SPI_CR1_BR_Msk) | presc;
	spiEnable(spi);
}

void spiEnable(SPI_TypeDef *spi)
{
	spi->CR1 |= SPI_CR1_SPE;
}

void spiDisable(SPI_TypeDef *spi)
{
	spi->CR1 &= ~SPI_CR1_SPE;
}

uint16_t spiExchangeData(SPI_TypeDef *spi, uint16_t data)
{
	spiWaitUntilSpiTransmits(spi);
	spi->DR = data;
	spiWwaitUntilSpiReceive(spi);
	return spi->DR;
}

void spiSendBuffer(SPI_TypeDef *spi, uint16_t *data, uint32_t n)
{
	uint16_t dummy;

	while(n)
	{
		dummy = spiExchangeData(spi, *data);
		data++;
		n--;
	}
	spiWaitUntilSpiTransmits(spi);
	spiWwaitUntilSpiIsBusy(spi);
}

void spiReceiveBuffer(SPI_TypeDef *spi, uint16_t *data, uint32_t n)
{
	const uint16_t DUMMY_DATA = 0xFFFF;

	while(n)
	{
		*data = spiExchangeData(spi, DUMMY_DATA);
		data++;
		n--;
	}
	spiWaitUntilSpiTransmits(spi);
	spiWwaitUntilSpiIsBusy(spi);
}

void spiExchangeBuffers(SPI_TypeDef *spi, uint16_t *dataSend, uint16_t *dataReceive, int32_t n)
{
	while(n)
	{
		*dataReceive = spiExchangeData(spi, *dataReceive);
		dataSend++;
		dataReceive++;
		n--;
	}
	spiWaitUntilSpiTransmits(spi);
	spiWwaitUntilSpiIsBusy(spi);
}

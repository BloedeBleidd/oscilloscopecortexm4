/*
File:		gpio.cpp
Author:		BloedeBleidd - Piotr �muda
Date:		27.06.2017
*/

#include "gpio.h"

void gpioInitialize(void)
{
	// enable all possible GPIO ports
#ifdef STM32F0
	RCC->AHBENR |= RCC_AHBENR_GPIOAEN | RCC_AHBENR_GPIOBEN | RCC_AHBENR_GPIOFEN;
#endif

#ifdef STM32F1
	RCC->APB2ENR |= RCC_APB2ENR_IOPAEN | RCC_APB2ENR_IOPBEN | RCC_APB2ENR_IOPCEN;// |
				//	RCC_APB2ENR_IOPDEN | RCC_APB2ENR_IOPEEN | RCC_APB2ENR_IOPFEN |
				//	RCC_APB2ENR_IOPGEN;
#endif

#ifdef STM32F4
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN | RCC_AHB1ENR_GPIOBEN | RCC_AHB1ENR_GPIOCEN |
					RCC_AHB1ENR_GPIODEN | RCC_AHB1ENR_GPIOEEN | RCC_AHB1ENR_GPIOFEN |
					RCC_AHB1ENR_GPIOGEN | RCC_AHB1ENR_GPIOHEN | RCC_AHB1ENR_GPIOIEN;
#endif
}

GpioResult gpioPinConfiguration(GPIO_TypeDef * const __restrict__ GPIOx, PinNumber pin, GpioMode mode)
{
	volatile uint32_t tmp;
	if(pin>=16)	return GPIO_ERROR;

#if defined(STM32L0) || defined(STM32L1) || defined(STM32L4) || defined (STM32F2) || defined (STM32F3) || defined(STM32F4) || defined(STM32F7) || defined(STM32H7)
	tmp = GPIOx->MODER;
	tmp &= ~(0x03 << pin*2);
	tmp |= (((mode>>5) & 0x03) << (pin*2));
	GPIOx->MODER = tmp;

	tmp = GPIOx->OTYPER;
	tmp &= ~(0x01 << pin);
	tmp |= (((mode>>4) & 0x01) << (pin*1));
	GPIOx->OTYPER = tmp;

	tmp = GPIOx->OSPEEDR;
	tmp &= ~(0x03 << pin*2);
	tmp |= (((mode>>2) & 0x03) << (pin*2));
	GPIOx->OSPEEDR = tmp;

	tmp = GPIOx->PUPDR;
	tmp &= ~(0x03 << pin*2);
	tmp |= (((mode>>0) & 0x03) << (pin*2));
	GPIOx->PUPDR = tmp;

#elif defined(STM32F1)
	uint32_t volatile * crReg;

	crReg = &GPIOx->CRL;

	if (pin > 7)
	{
		pin -= 8;
		crReg = &GPIOx->CRH;
	}

	tmp = *crReg;
	tmp &= ~((uint32_t)(0x0f << (pin*4)));
	tmp |= (uint32_t)(mode << (pin*4));
	*crReg = tmp;
#endif

	return GPIO_OK;
}

GpioResult gpioPinAlternateFunctionConfiguration(GPIO_TypeDef * const __restrict__ GPIOx, PinNumber pin, AlternateFunctionNumber af)
{
	if(pin>=16)	return GPIO_ERROR;

#if defined(STM32L0) || defined(STM32L1) || defined(STM32L4) || defined (STM32F2) || defined (STM32F3) || defined(STM32F4) || defined(STM32F7) || defined(STM32H7)
	uint32_t tmp;
	uint8_t i = 0;

	if(pin>=8)
	{
		pin -= 8;
		i = 1;
	}

	tmp = GPIOx->AFR[i];
	tmp &= ~(0x0F << (pin*4));
	tmp |= af << (pin*4);
	GPIOx->AFR[i] = tmp;

#elif defined(STM32F1)
	return false;
#endif

	return GPIO_OK;
}

void gpioBitSet(GPIO_TypeDef * const __restrict__ GPIOx, PinNumber gpio_pin)
{
#if defined(STM32F0) || defined(STM32F1)
	GPIOx->BSRR = 1 << gpio_pin;
#endif

#ifdef STM32F4
	GPIOx->BSRR = 1 << gpio_pin;
#endif
}

void gpioBitReset(GPIO_TypeDef * const __restrict__ GPIOx, PinNumber gpio_pin)
{
#if defined(STM32F0) || defined(STM32F1)
	GPIOx->BRR = 1 << gpio_pin;
#endif

#ifdef STM32F4
	GPIOx->BSRR = 1 << (gpio_pin+16);
#endif
}

void gpioBitWrite(GPIO_TypeDef * const __restrict__ GPIOx, PinNumber gpio_pin, GpioPinValue value)
{
	if(value == GPIO_TRUE)
	{
#if defined(STM32F0) || defined(STM32F1)
		GPIOx->BSRR = 1 << gpio_pin;
#endif

#ifdef STM32F4
		GPIOx->BSRR = gpio_pin;
#endif
	}
	else
	{
#if defined(STM32F0) || defined(STM32F1)
		GPIOx->BRR = 1 << gpio_pin;
#endif

#ifdef STM32F4
		GPIOx->BSRR = (uint32_t)gpio_pin << 16U;
#endif
	}
}

void gpioBitToggle(GPIO_TypeDef * const __restrict__ GPIOx, PinNumber gpio_pin)
{
  GPIOx->ODR ^= 1 << gpio_pin;
}

GpioPinValue gpioBitRead(GPIO_TypeDef * const __restrict__ GPIOx, PinNumber gpio_pin)
{
	if ((GPIOx->IDR & (1 << gpio_pin)) == GPIO_FALSE)
	{
		return GPIO_FALSE;
	}
	else
	{
		return GPIO_TRUE;
	}
}

void gpioPortWrite(GPIO_TypeDef * const __restrict__ GPIOx, GpioPort value)
{
	GPIOx->ODR = value;
}

GpioPort gpioPortRead(GPIO_TypeDef * const __restrict__ GPIOx)
{
	return ((uint16_t)GPIOx->IDR);
}

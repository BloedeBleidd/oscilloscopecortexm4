/*
 * tasks_include.h
 *
 *  Created on: 07.08.2017
 *      Author: BloedeBleidd
 */

#ifndef TASKS_INCLUDE_H_
#define TASKS_INCLUDE_H_

#include "task_gui.h"
#include "task_status_led.h"
#include "task_touch_panel.h"
#include "task_measurement.h"
#include "task_hardware_measurement.h"


#endif /* TASKS_INCLUDE_H_ */

/*
 * task_main.h
 *
 *  Created on: 25.08.2017
 *      Author: BloedeBleidd
 */

#ifndef TASK_GUI_H_
#define TASK_GUI_H_

#include <stdint.h>

// (O0)
const uint16_t stackGui = 20000;
// (Os)
//const uint16_t stackGui = 10000;

void taskGui ( void * param );


#endif /* TASK_GUI_H_ */

/*
 * task_hardware_measurement.h
 *
 *  Created on: May 18, 2018
 *      Author: Y520
 */

#ifndef TASK_HARDWARE_MEASUREMENT_H_
#define TASK_HARDWARE_MEASUREMENT_H_


#include <stdint.h>

// (O0)
const uint16_t stackHardwareMeasurement = 200;
// (Os)
//const uint16_t stackHardwareMeasurement = 10000;

void taskHardwareMeasurement ( void * param );

enum HardwareReady { CH1_READY, CH2_READY };

struct HardwareSettingsData
{
	uint32_t ch, vol, tim;
};

extern QueueHandle_t queueHardwareReady;
extern QueueHandle_t queueHardwareSettings;

#endif /* TASK_HARDWARE_MEASUREMENT_H_ */

/*
 * task_measurement.h
 *
 *  Created on: May 18, 2018
 *      Author: Y520
 */

#ifndef TASK_MEASUREMENT_H_
#define TASK_MEASUREMENT_H_


#include <stdint.h>

// (O0)
const uint16_t stackMeasurement = 2000;
// (Os)
//const uint16_t stackMeasurement = 10000;

void taskMeasurement ( void * param );


#endif /* TASK_MEASUREMENT_H_ */

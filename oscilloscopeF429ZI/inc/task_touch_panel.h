/*
 * touch_panel.h
 *
 *  Created on: 07.08.2017
 *      Author: BloedeBleidd
 */

#ifndef TASK_TOUCH_PANEL_H_
#define TASK_TOUCH_PANEL_H_

#include <cstdio>
#include <stdint.h>
#include "RTOS_includes.h"

struct TouchPanelCoordinates
{
	uint16_t x, y;
};

// (O0)
//const uint16_t stackTouchPanel = 79;
// (O3)
const uint16_t stackTouchPanel = 100;

extern QueueHandle_t queueTouchPanel;

void taskTouchPanel ( void * param );

#endif /* TASK_TOUCH_PANEL_H_ */

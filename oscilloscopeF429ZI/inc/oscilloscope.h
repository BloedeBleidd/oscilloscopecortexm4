/*
 * channel.h
 *
 *  Created on: May 1, 2018
 *      Author: Y520
 */

#ifndef OSCILLOSCOPE_H_
#define OSCILLOSCOPE_H_

#include "graphic.h"
#include "task_touch_panel.h"

class Oscilloscope
{
public:
	enum Channel { CHANNEL1=0, CHANNEL2, BOTH };
	enum Trigger { NONE=0, AUTO_RISING, AUTO_FALLING, NORMAL_RISING, NORMAL_FALLING };
private:
	enum Voltage { v10=0, v5, v2, v1, v500m, v200m };
	enum Time { s5=0, s2, s1, s500m, s200m, s100m, s50m, s20m, s10m, s5m, s2m, s1m, s500u, s200u, s100u, s50u, s20u, s10u, s5u };

	struct ChannelData
	{
		Trigger trigger;
		uint32_t voltage;
		uint32_t time;
		uint8_t *data;
		Color color;
		uint32_t triggerXPos;
		uint32_t triggerYPos;
		bool toDisplay;
	};

	static const char voltageStr[6][8];
	static const char timeStr[19][8];

	Channel settings = CHANNEL1;
	ChannelData ch1, ch2;
	size_t buffSize;
	Color triggerColor;

	size_t findEdge( ChannelData ch );
	void findTrigger( ChannelData ch );
	void drawWave( Graphic &lcd, ChannelData ch );
	void drawTrigger( Graphic &lcd, ChannelData ch );

public:
	Oscilloscope( uint8_t *data1, uint8_t *data2, size_t n, Color col1, Color col2, Color colTr );

	void draw( Graphic &lcd, Channel ch );
	void update( TouchPanelCoordinates tp );

};


#endif /* OSCILLOSCOPE_H_ */

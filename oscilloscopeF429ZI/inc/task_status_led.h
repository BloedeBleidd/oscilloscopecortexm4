/*
 * status_led.h
 *
 *  Created on: 07.08.2017
 *      Author: BloedeBleidd
 */

#ifndef TASK_STATUS_LED_H_
#define TASK_STATUS_LED_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

// (O0)
//const uint16_t stackStatusLed = 52;
// (O3)
const uint16_t stackStatusLed = 100;

void taskStatusLed ( void * param );

#ifdef __cplusplus
}
#endif


#endif /* TASK_STATUS_LED_H_ */

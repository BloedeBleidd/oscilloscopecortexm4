/*
 * hardware.h
 *
 *  Created on: Apr 29, 2018
 *      Author: Y520
 */

#ifndef HARDWARE_H_
#define HARDWARE_H_

#include "stm32f4xx.h"
#include "RTOS_includes.h"
#include "gpio.h"

#define CH1_GPIO GPIOA
#define CH2_GPIO GPIOA
#define CH1_PIN 7
#define CH2_PIN 5
#define CH1_TIM TIM2
#define CH2_TIM TIM3
#define CH1_ADC ADC1
#define CH2_ADC ADC2
#define CH12_DMA DMA2
#define CH1_DMA_STREAM DMA2_Stream0
#define CH2_DMA_STREAM DMA2_Stream3

const uint32_t BUFFER_SIZE = 1000;
extern uint8_t signalBuffer1 [BUFFER_SIZE];
extern uint8_t signalBuffer2 [BUFFER_SIZE];

void timerStop( TIM_TypeDef *tim );
void timerStart( TIM_TypeDef *tim );
void timerConfig( TIM_TypeDef *tim, uint32_t cnt, uint32_t psc, uint32_t arr );
void timerUpdateEventConfig( TIM_TypeDef *tim );
void adcDmaRestart( ADC_TypeDef *adc );
void dmaConfig( DMA_Stream_TypeDef *dmaStr, ADC_TypeDef *adc, void* ptr );
bool dmaCh1ReadInterruptFlag();
void dmaCh1ClearInterruptFlag();
bool dmaCh2ReadInterruptFlag();
void dmaCh2ClearInterruptFlag();
void initHardware();


#endif /* HARDWARE_H_ */

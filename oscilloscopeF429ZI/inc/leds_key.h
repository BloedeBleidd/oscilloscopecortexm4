/*
 * leds_key.h
 *
 *  Created on: 31.07.2017
 *      Author: BloedeBleidd
 */

#ifndef LEDS_HEY_H_
#define LEDS_HEY_H_

#define KEY_GPIO		GPIOA
#define KEY_PIN			0

#define LED_GREEN_GPIO	GPIOG
#define LED_GREEN_PIN	13

#define LED_RED_GPIO	GPIOG
#define LED_RED_PIN		14


#endif /* LEDS_HEY_H_ */

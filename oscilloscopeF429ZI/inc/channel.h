/*
 * channel.h
 *
 *  Created on: May 2, 2018
 *      Author: Y520
 */

#ifndef CHANNEL_H_
#define CHANNEL_H_



#include <cstddef>
#include <cstdint>

class Channel
{
public:
    enum TriggerMode { AUTO=0, NORMAL };
    enum TriggerEdge { RISING=0, FALLING, BOTH };
private:
    struct Cursor { int cur1, cur2; };
    struct Trigger { int x, y; };
    static const size_t VOLTAGE_SIZE = 6;
    static const size_t TIME_SIZE = 19;
    static const size_t TRIGGER_MODE_SIZE = 2;
    static const size_t TRIGGER_EDGE_SIZE = 3;
    static const size_t STRING_SIZE = 10;
    static const char voltageString[VOLTAGE_SIZE][STRING_SIZE];
    static const char timeString[TIME_SIZE][STRING_SIZE];
    static const char triggerModeString[TRIGGER_MODE_SIZE][STRING_SIZE];
    static const char triggerEdgeString[TRIGGER_EDGE_SIZE][STRING_SIZE];

    TriggerMode triggerMode = AUTO;
    TriggerEdge triggerEdge = RISING;
    Trigger triggerPosition = { 0, 0 };
    Cursor cursorVoltage = { 0, 0 }, cursorTime = { 0, 0 };
    size_t voltage = 0;
    size_t time = 0;
    uint8_t *data = nullptr;
    size_t dataSize;
    bool toDisplay = false;
    size_t width, height;

    size_t findEdgeRising();
    size_t findEdgeFalling();
    size_t findEdge();

public:
    Channel( size_t bufferSize, size_t _width, size_t _height );

    void setTriggerMode( TriggerMode mode )                     { triggerMode = mode; }
    void setTriggerEdge( TriggerEdge edge )                     { triggerEdge = edge; }
    void setTriggerPosition( Trigger position )                 { triggerPosition = position; }
    void setCursorTime( Cursor time )                           { cursorTime = time; }
    void setCursorVoltage( Cursor voltage )                     { cursorVoltage = voltage; }
    bool setVoltageSensitivity( size_t sensitivity );
    bool setTimeScale( size_t scale );

    TriggerMode getTriggerMode() const                          { return triggerMode; }
    TriggerEdge getTriggerEdge() const                          { return triggerEdge; }
    Trigger getTriggerPosition() const                          { return triggerPosition; }
    Cursor getCursorTime() const                                { return cursorTime; }
    Cursor getCursorVoltage() const                             { return cursorVoltage; }
    const char* getStringTimeBase() const                       { return timeString[time]; }
    const char* getStringVoltageSensitivity() const             { return voltageString[voltage]; }
    const char* getStringTimeBase( size_t n ) const             { return timeString[n]; }
    const char* getStringVoltageSensitivity( size_t n ) const   { return voltageString[n]; }
    const uint8_t* getData() const                              { return data; }

    void copyData( uint8_t *newData );
    void findTrigger();
    bool isReadyToDisplay() const                               { return toDisplay; }
    void resizeDataBuffer( size_t newSize );
};


#endif /* CHANNEL_H_ */

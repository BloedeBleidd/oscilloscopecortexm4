/*
 * channel.cpp
 *
 *  Created on: May 11, 2018
 *      Author: Y520
 */


#include "channel.h"

const char Channel::voltageString[VOLTAGE_SIZE][STRING_SIZE] = { "10V/div", "5V/div", "2V/div", "1V/div", "500mV/div", "200mV/div" };
const char Channel::timeString[TIME_SIZE][STRING_SIZE] =  {   "10s/div", "5s/div", "2s/div", "1s/div", "500ms/div", "200ms/div", "100ms/div", "50ms/div", "20ms/div", "10ms/div",
                                                            "5ms/div", "2ms/div", "1ms/div", "500us/div", "200us/div", "100us/div", "50us/div", "20us/div", "10us/div" };
const char Channel::triggerEdgeString[TRIGGER_EDGE_SIZE][STRING_SIZE] = { "RISING", "FALLING", "BOTH" };
const char Channel::triggerModeString[TRIGGER_MODE_SIZE][STRING_SIZE] = { "AUTO", "NORMAL" };


Channel::Channel(size_t bufferSize, size_t _width, size_t _height )
: dataSize(bufferSize), width(_width), height(_height)
{
    data = new uint8_t[bufferSize];
}

size_t Channel::findEdgeRising()
{
    size_t pos = width/2;

    for( ; pos<dataSize && data[pos] <= triggerPosition.y; pos++ ) {}
    for( ; pos<dataSize && data[pos] >= triggerPosition.y; pos++ ) {}

    return pos;
}

size_t Channel::findEdgeFalling()
{
    size_t pos = width/2;

    for( ; pos<dataSize && data[pos] >= triggerPosition.y; pos++ ) {}
    for( ; pos<dataSize && data[pos] <= triggerPosition.y; pos++ ) {}

    return pos;
}

size_t Channel::findEdge()
{
    size_t pos;

    if( triggerEdge == RISING )
        pos = findEdgeRising();
    else if( triggerEdge == FALLING )
        pos = findEdgeFalling();
    else if( triggerEdge == BOTH )
    {
        pos = findEdgeRising();
        if( pos >= width )
            pos = findEdgeFalling();
    }

    if( pos >= dataSize-width/2 )
        pos = width/2;

    return pos;
}

void Channel::findTrigger()
{
    triggerPosition.x = findEdge();

    if( triggerMode == AUTO )
        toDisplay = true;
    else if( triggerMode == NORMAL )
    {
        if( triggerPosition.x == (int)( width/2 ) )
            toDisplay = false;
        else
            toDisplay = true;
    }

}

 void Channel::copyData( uint8_t *newData )
 {
     for( size_t i=0; i<dataSize; i++, data++, newData++ )
        *data = *newData;
 }

 void Channel::resizeDataBuffer( size_t newSize )
 {
     delete[] data;
     data = new uint8_t[newSize];
 }



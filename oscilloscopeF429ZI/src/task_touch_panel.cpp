/*
 * touch_panel.cpp
 *
 *  Created on: 07.08.2017
 *      Author: BloedeBleidd
 */

/****************************** Includes ******************************/
#include "task_touch_panel.h"
#include "RTOS_includes.h"
#include "stmpe811.h"

/*************************************** Private types ***************************************/

/*************************************** Private constants ***************************************/
const uint16_t queueTouchPanel_SIZE = 32;

/*************************************** Private integers ***************************************/

/*************************************** Private function prototypes ***************************************/

/*************************************** Queues ****************************************/
QueueHandle_t  queueTouchPanel;

/*************************************** Tasks ****************************************/
void taskTouchPanel ( void * param )
{
	/******************** private task constants ********************/
	const uint16_t WIDTH = 320, HEIGHT = 240;
	const uint16_t DELAY = 25;
	const Stmpe811::I2CInitStruct initStruct
	{
		I2C3, 4,
		GPIOA, 8,
		GPIOC, 9
	};

	/******************** private task variables ********************/
	TickType_t lastTime;
	TouchPanelCoordinates coord;
	#if INCLUDE_uxTaskGetStackHighWaterMark > 0
	volatile UBaseType_t uxHighWaterMark;
	#endif

	/******************** private task initializations ********************/
	queueTouchPanel = xQueueCreate( queueTouchPanel_SIZE, sizeof( TouchPanelCoordinates ) );

	RCC->APB1ENR |= RCC_APB1ENR_I2C3EN;
	__DSB();
	Stmpe811 touchPanel( WIDTH, HEIGHT, TouchPanel::Landscape_2, initStruct );
	touchPanel.calibrate(30, 305, 15, 225);
	lastTime = xTaskGetTickCount();

	/******************** private task infinite loop ********************/
	for(;;)
	{
		#if INCLUDE_uxTaskGetStackHighWaterMark > 0
		uxHighWaterMark = uxTaskGetStackHighWaterMark( NULL );
		#endif

		vTaskDelayUntil( &lastTime, pdMS_TO_TICKS(DELAY) );
		if(touchPanel.update())
		{
			coord.x  = touchPanel.getX();
			coord.y = touchPanel.getY();
			xQueueSend( queueTouchPanel, ( void * ) &coord, ( TickType_t ) 0 );
		}
	}
}

/*************************************** Private functions ****************************************/

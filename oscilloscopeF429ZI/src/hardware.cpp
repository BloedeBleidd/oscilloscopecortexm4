/*
 * task_main.cpp
 *
 *  Created on: 25.08.2017
 *      Author: BloedeBleidd
 */

#include "hardware.h"

void timerStop( TIM_TypeDef *tim )
{
	tim->CR1 &= ~TIM_CR1_CEN;
}

void timerStart( TIM_TypeDef *tim )
{
	tim->CR1 |= TIM_CR1_CEN;
}

void timerConfig( TIM_TypeDef *tim, uint32_t cnt, uint32_t psc, uint32_t arr )
{
	tim->CNT = cnt;
	tim->PSC = psc;
	tim->ARR = arr;
	tim->EGR = TIM_EGR_UG;
}

void timerUpdateEventConfig( TIM_TypeDef *tim )
{
	tim->CR2 = TIM_CR2_MMS_1;
}

void adcDmaRestart( ADC_TypeDef *adc )
{
	adc->CR2 &= ~ADC_CR2_DMA;
	adc->CR2 |= ADC_CR2_DMA;
}

void dmaConfig( DMA_Stream_TypeDef *dmaStr, ADC_TypeDef *adc, void* ptr )
{
	dmaStr->PAR = (uint32_t)&adc->DR;
	dmaStr->M0AR = (uint32_t)ptr;
	dmaStr->NDTR = BUFFER_SIZE;
	dmaStr->CR = DMA_SxCR_MINC | DMA_SxCR_EN | DMA_SxCR_TCIE;
}

bool dmaCh1ReadInterruptFlag()
{
	return CH12_DMA->LISR & DMA_LISR_TCIF0;
}

void dmaCh1ClearInterruptFlag()
{
	CH12_DMA->LIFCR |= DMA_LIFCR_CTCIF0;
}

bool dmaCh2ReadInterruptFlag()
{
	return CH12_DMA->LISR & DMA_LISR_TCIF3;
}

void dmaCh2ClearInterruptFlag()
{
	CH12_DMA->LIFCR |= DMA_LIFCR_CTCIF3;
}

void initHardware()
{
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN | RCC_AHB1ENR_DMA2EN;
	RCC->APB1ENR |= RCC_APB1ENR_TIM2EN | RCC_APB1ENR_TIM3EN;
	RCC->APB2ENR |= RCC_APB2ENR_ADC1EN | RCC_APB2ENR_ADC2EN;
	__DSB();

	gpioPinConfiguration( CH1_GPIO, CH1_PIN, GPIO_MODE_ANALOG );
	gpioPinConfiguration( CH2_GPIO, CH2_PIN, GPIO_MODE_ANALOG );

	timerConfig( CH1_TIM, 0, 0, 14999 );
	timerUpdateEventConfig( CH1_TIM );
	timerConfig( CH2_TIM, 0, 0, 14999 );
	timerUpdateEventConfig( CH2_TIM );

	//dmaCh1Config();
	//dmaCh2Config();

	CH1_ADC->CR1 = ADC_CR1_RES;
	CH1_ADC->CR2 = ADC_CR2_ADON | ADC_CR2_DMA | ADC_CR2_EXTEN_0 | ADC_CR2_EXTSEL_1 | ADC_CR2_EXTSEL_2; // tim2 tr
	vTaskDelay( 5 );
	CH1_ADC->SQR3 = 7<<0;

	CH2_ADC->CR1 = ADC_CR1_RES;
	CH2_ADC->CR2 = ADC_CR2_ADON | ADC_CR2_DMA | ADC_CR2_EXTEN_0 | ADC_CR2_EXTSEL_3; //tim3 tr
	vTaskDelay( 5 );
	CH2_ADC->SQR3 = 5<<0;
}

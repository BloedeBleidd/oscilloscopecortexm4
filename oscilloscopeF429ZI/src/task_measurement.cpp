/*
 * task_main.cpp
 *
 *  Created on: 25.08.2017
 *      Author: BloedeBleidd
 */

/****************************** Includes ******************************/
#include "stm32f4xx.h"
#include "RTOS_includes.h"
#include "sdram.h"
#include "tasks_include.h"


/*************************************** Private types ***************************************/

/*************************************** Private constants ***************************************/

/*************************************** Private integers ***************************************/

/*************************************** Private function prototypes ***************************************/

/*************************************** Queues ****************************************/

/*************************************** Tasks ****************************************/
void taskMeasurement ( void * param )
{
	/******************** private task constants ********************/

	/******************** private task variables ********************/
	#if INCLUDE_uxTaskGetStackHighWaterMark > 0
	volatile UBaseType_t uxHighWaterMark;
	#endif

	/******************** private task initializations ********************/

	/******************** private task infinite loop ********************/
	for(;;)
	{
		vTaskDelay( 1000 );

		#if INCLUDE_uxTaskGetStackHighWaterMark > 0
		uxHighWaterMark = uxTaskGetStackHighWaterMark( NULL );
		#endif
	}
}

/*************************************** Private functions ****************************************/

/*
 * status_led.c
 *
 *  Created on: 07.08.2017
 *      Author: BloedeBleidd
 */

/****************************** Includes ******************************/
#include "task_status_led.h"
#include "RTOS_includes.h"
#include "gpio.h"
#include "leds_key.h"

/*************************************** Private types ***************************************/

/*************************************** Private constants ***************************************/

/*************************************** Private integers ***************************************/

/*************************************** Private function prototypes ***************************************/

/*************************************** Queues ****************************************/

/*************************************** Tasks ****************************************/
void taskStatusLed ( void * param )
{
	/******************** private task constants ********************/
	const TickType_t DELAY = 250;

	/******************** private task variables ********************/
	TickType_t lastTime;
	#if INCLUDE_uxTaskGetStackHighWaterMark > 0
	volatile UBaseType_t uxHighWaterMark;
	#endif

	/******************** private task initializations ********************/
	gpioPinConfiguration(LED_GREEN_GPIO, LED_GREEN_PIN, GPIO_MODE_OUTPUT_PUSH_PULL_LOW_SPEED);
	lastTime = xTaskGetTickCount();

	/******************** private task infinite loop ********************/
	for(;;)
	{
		#if INCLUDE_uxTaskGetStackHighWaterMark > 0
		uxHighWaterMark = uxTaskGetStackHighWaterMark( NULL );
		#endif

		vTaskDelayUntil( &lastTime, pdMS_TO_TICKS(DELAY) );
		gpioBitToggle(LED_GREEN_GPIO, LED_GREEN_PIN);
	}

}

/*************************************** Private functions ****************************************/


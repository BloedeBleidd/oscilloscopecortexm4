/*
 * task_main.cpp
 *
 *  Created on: 25.08.2017
 *      Author: BloedeBleidd
 */

/****************************** Includes ******************************/
#include "stm32f4xx.h"
#include "RTOS_includes.h"
#include "sdram.h"
#include "ili9341_ltdc.h"
#include "tasks_include.h"
#include "leds_key.h"
#include "hardware.h"
#include "channel.h"
#include "bitmaps.h"
#include "touch_panel.h"


/*************************************** Private types ***************************************/
enum ChannelNumber { CH1, CH2 };

/*************************************** Private constants ***************************************/

/*************************************** Private integers ***************************************/

/*************************************** Private function prototypes ***************************************/
void hardwareTime( TIM_TypeDef *tim, size_t n );
void drawChannel( Graphic &lcd, const Channel &ch, Color col );
void drawBackground( Graphic &lcd );
void drawSettings( Graphic &lcd, const Channel &ch );
bool refreshSettings( Channel &ch, const TouchPanelCoordinates &touch );
Channel& referToActualChannel( Channel &ch1, Channel &ch2, ChannelNumber curr );

/*************************************** Queues ****************************************/

/*************************************** Tasks ****************************************/
void taskGui ( void * param )
{
	/******************** private task constants ********************/
	const uint32_t VRAM_SIZE = 320*240*2;
	const uint32_t GRAM = SDRAM_END-VRAM_SIZE, VRAM1 = SDRAM_END-VRAM_SIZE*2;//, VRAM2 = SDRAM_END-VRAM_SIZE*3;
	const Ili9341_LTDC::SpiInitStruct lcdStruct	{	SPI5, 5,	GPIOC, 2,	GPIOD, 13,	GPIOF, 9,	GPIOF, 7	};

	/******************** private task variables ********************/
	#if INCLUDE_uxTaskGetStackHighWaterMark > 0
	volatile UBaseType_t uxHighWaterMark;
	#endif

	bool toDisplay = true;
	HardwareReady ready;
	HardwareSettingsData settingsData;
	TouchPanelCoordinates touch;
	ChannelNumber currentSettingsChannel = CH1;

	/******************** private task initializations ********************/
	RCC->APB2ENR |= RCC_APB2ENR_SPI5EN;
	Ili9341_LTDC lcd(lcdStruct, (Color*)GRAM, (void*)VRAM1, Graphic::Landscape_1);//, (void*)VRAM2);
	lcd.layerOnBottom();
	lcd.fontSet(Font_7x10);

	Channel ch1( BUFFER_SIZE, 300, 150 );
	Channel ch2( BUFFER_SIZE, 300, 150 );

	settingsData = { 1, 1, 1000 };
	xQueueSend( queueHardwareSettings, &settingsData, 0 );
	settingsData = { 2, 1, 2000 };
	xQueueSend( queueHardwareSettings, &settingsData, 0 );

	/******************** private task infinite loop ********************/
	for(;;)
	{
		if( pdTRUE == xQueueReceive( queueHardwareReady, (void*) &ready, 0 ) )
		{
			if( CH1_READY == ready )
			{
				ch1.copyData( signalBuffer1 );
				settingsData = { 1, 1, 1000 };
				xQueueSend( queueHardwareSettings, &settingsData, 0 );
				ch1.findTrigger();
				if( true == ch1.isReadyToDisplay() )
					toDisplay = true;
			}
			else if( CH2_READY == ready )
			{
				ch2.copyData( signalBuffer2 );
				settingsData = { 2, 1, 2000 };
				xQueueSend( queueHardwareSettings, &settingsData, 0 );
				ch2.findTrigger();
				if( true == ch2.isReadyToDisplay() )
					toDisplay = true;
			}
		}

		if( true == toDisplay )
		{
			toDisplay = false;

			drawBackground( lcd );

			if( true == ch1.isReadyToDisplay() )
				drawChannel( lcd, ch1, ORANGE );

			if( true == ch2.isReadyToDisplay() )
				drawChannel( lcd, ch2, PINK );

			drawSettings( lcd, referToActualChannel( ch1, ch2, currentSettingsChannel ) );

			lcd.refreshCurrentLayer();
		}

		if( pdTRUE == xQueueReceive( queueTouchPanel, (void*) &touch, 0 ) )
		{
			bool anyChanges = refreshSettings( referToActualChannel( ch1, ch2, currentSettingsChannel ), touch );

			if( true == anyChanges )
				toDisplay = anyChanges;

			xQueueReset( queueTouchPanel );
		}

		#if INCLUDE_uxTaskGetStackHighWaterMark > 0
		uxHighWaterMark = uxTaskGetStackHighWaterMark( NULL );
		#endif
	}
}

/*************************************** Private functions ****************************************/

void drawChannel( Graphic &lcd, const Channel &ch, Color col )
{
	for( uint32_t i=1; i<298; i++ )
		lcd.drawLine( i, ch.getData()[i+ch.getTriggerPosition().x], i+1, ch.getData()[i+1+ch.getTriggerPosition().x], col );
}

void drawBackground( Graphic &lcd )
{
	lcd.drawBitmap16Bit( 0, 16, background, 300, 210 );
}

void drawSettings( Graphic &lcd, const Channel &ch )
{

}

bool refreshSettings( Channel &ch, const TouchPanelCoordinates &touch )
{
	//hardwareTime();
	return false;
}

Channel& referToActualChannel( Channel &ch1, Channel &ch2, ChannelNumber curr )
{
	if( CH1 == curr )
		return ch1;
	else
		return ch2;
}

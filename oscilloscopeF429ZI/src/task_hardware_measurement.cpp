/*
 * task_main.cpp
 *
 *  Created on: 25.08.2017
 *      Author: BloedeBleidd
 */

/****************************** Includes ******************************/
#include "stm32f4xx.h"
#include "RTOS_includes.h"
#include "tasks_include.h"
#include "hardware.h"
#include "semphr.h"


/*************************************** Private types ***************************************/

/*************************************** Private constants ***************************************/

/*************************************** Private integers ***************************************/
uint8_t signalBuffer1 [BUFFER_SIZE];
uint8_t signalBuffer2 [BUFFER_SIZE];

/*************************************** Private function prototypes ***************************************/
uint32_t calculateARR( uint32_t t );
uint32_t calculatePSC( uint32_t t );
void setVoltagePins( uint32_t v, uint32_t c );
void channelConfig( TIM_TypeDef *tim, ADC_TypeDef *adc, DMA_Stream_TypeDef *dmaStr, void *ptr, uint32_t t, uint32_t v );

/*************************************** Queues ****************************************/
QueueHandle_t queueHardwareReady = NULL;
QueueHandle_t queueHardwareSettings = NULL;

/*************************************** Tasks ****************************************/
void taskHardwareMeasurement ( void * param )
{
	/******************** private task constants ********************/

	/******************** private task variables ********************/
	#if INCLUDE_uxTaskGetStackHighWaterMark > 0
	volatile UBaseType_t uxHighWaterMark;
	#endif
	HardwareSettingsData data;

	/******************** private task initializations ********************/
	initHardware();
	NVIC_EnableIRQ( DMA2_Stream0_IRQn );
	NVIC_SetPriority(DMA2_Stream0_IRQn, 11 );
	NVIC_EnableIRQ( DMA2_Stream3_IRQn );
	NVIC_SetPriority(DMA2_Stream3_IRQn, 12 );

	queueHardwareReady = xQueueCreate( 10, sizeof( HardwareReady ) );
	queueHardwareSettings = xQueueCreate( 10, sizeof( HardwareSettingsData ) );

	/******************** private task infinite loop ********************/
	for(;;)
	{
		if( pdTRUE == xQueueReceive( queueHardwareSettings, &data, 0xffff ) )
		{
			//taskENTER_CRITICAL();
			if( 1 == data.ch )
				channelConfig( CH1_TIM, CH1_ADC, CH1_DMA_STREAM, signalBuffer1, data.tim, data.vol );
			else if( 2 == data.ch )
				channelConfig( CH2_TIM, CH2_ADC, CH2_DMA_STREAM, signalBuffer2, data.tim, data.vol );
			//taskEXIT_CRITICAL();
		}

		#if INCLUDE_uxTaskGetStackHighWaterMark > 0
		uxHighWaterMark = uxTaskGetStackHighWaterMark( NULL );
		#endif
	}
}

/*************************************** Private functions ****************************************/

extern "C"
{
void DMA2_Stream0_IRQHandler( void )
{
	BaseType_t xHigherPriorityTaskWoken;

	if( true == dmaCh1ReadInterruptFlag() )
	{
		dmaCh1ClearInterruptFlag();
		timerStop( CH1_TIM );
	}

	HardwareReady cmd = CH1_READY;
	xQueueSendFromISR( queueHardwareReady, &cmd, &xHigherPriorityTaskWoken );
	portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
}

void DMA2_Stream3_IRQHandler( void )
{
	BaseType_t xHigherPriorityTaskWoken;

	if( true == dmaCh2ReadInterruptFlag() )
	{
		dmaCh2ClearInterruptFlag();
		timerStop( CH2_TIM );
	}

	HardwareReady cmd = CH2_READY;
	xQueueSendFromISR( queueHardwareReady, &cmd, &xHigherPriorityTaskWoken );
	portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
}
}

uint32_t calculateARR( uint32_t t )
{
	if( t > 20000 )
		return 3*t/1000-1;
	else
		return 3*t-1;
}

uint32_t calculatePSC( uint32_t t )
{
	if( t > 20000 )
		return 1000-1;
	else
		return 1-1;
}

void setVoltagePins( uint32_t v, uint32_t c )
{

}

void channelConfig( TIM_TypeDef *tim, ADC_TypeDef *adc, DMA_Stream_TypeDef *dmaStr, void *ptr, uint32_t t, uint32_t v )
{
	dmaConfig( dmaStr, adc, ptr );
	adcDmaRestart( adc );
	timerConfig( tim, 0, calculatePSC( t ), calculateARR( t ) );
	timerUpdateEventConfig( tim );
	timerStart( tim );
}

/*
 * channel.cpp
 *
 *  Created on: May 1, 2018
 *      Author: Y520
 */

#include "oscilloscope.h"
#include "bitmaps.h"
#include "colors.h"

const char Oscilloscope::voltageStr[6][8] = { "10V/d", "5V/d", "2V/d", "1V/d", "500mV/d", "200mV/d" };
const char Oscilloscope::timeStr[19][8] = { "5s/d", "2s/d", "1s/d", "500ms/d", "200ms/d", "100ms/d", "50ms/d", "20ms/d", "10ms/d", "5ms/d", "2ms/d", "1ms/d", "500us/d", "200us/d", "100us/d", "50us/d", "20us/d", "10us/d", "5us/d" };

Oscilloscope::Oscilloscope( uint8_t *data1, uint8_t *data2, size_t n, Color col1, Color col2, Color colTr  )
{
	triggerColor = colTr;
	buffSize = n;
	ch1.trigger = NONE;
	ch1.voltage = 0;
	ch1.time = 0;
	ch1.data = data1;
	ch1.color = col1;
	ch2.trigger = NONE;
	ch2.voltage = 0;
	ch2.time = 0;
	ch2.data = data2;
	ch2.color = col2;
}

void Oscilloscope::draw( Graphic &lcd, Channel ch )
{
	lcd.drawBitmap16Bit( 0, 15, background, 300, 210 );

	if( ch == CHANNEL1 || ch == BOTH )
		drawWave( lcd, ch1 );

	if( ch == CHANNEL2 || ch == BOTH )
		drawWave( lcd, ch2 );
}

void Oscilloscope::update( TouchPanelCoordinates tp )
{

}

void Oscilloscope::drawWave( Graphic &lcd, ChannelData ch )
{
	for( int i=1; i<298; i++ )
		lcd.drawLine( i, ch.data[i+ch.triggerXPos], i+1, ch.data[i+1+ch.triggerXPos], ch.color );
}

void Oscilloscope::drawTrigger( Graphic &lcd, ChannelData ch )
{
	for( int i=1; i<298; i+=5 )
		lcd.drawLineHorizontal( i, i+4, ch.triggerYPos, triggerColor );
}

size_t Oscilloscope::findEdge( ChannelData ch )
{
	size_t cursor = 149;

	if( ch.trigger == AUTO_RISING || ch.trigger == NORMAL_RISING )
	{
		while( ch.data[cursor++] <= ch.triggerYPos ) {}
		while( ch.data[cursor++] >= ch.triggerYPos ) {}
	}
	else if( ch.trigger == AUTO_FALLING || ch.trigger == NORMAL_FALLING )
	{
		while( ch.data[cursor++] >= ch.triggerYPos ) {}
		while( ch.data[cursor++] <= ch.triggerYPos ) {}
	}
	return cursor;
}

void Oscilloscope::findTrigger( ChannelData ch )
{
	size_t cursor = findEdge( ch );

	if( ch.trigger == NORMAL_FALLING || ch.trigger == NORMAL_RISING )
	{
		if( cursor>=buffSize-152 )
			ch.toDisplay = false;
	}
	else if( ch.trigger == AUTO_FALLING || ch.trigger == AUTO_RISING )
	{
		if( cursor>=buffSize-152 )
			cursor = 149;
		ch.toDisplay = true;
	}
	else if( ch.trigger == NONE )
		ch.toDisplay = true;

}





















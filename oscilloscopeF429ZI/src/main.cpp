
#include "stm32f4xx.h"
#include "tasks_include.h"
#include "RTOS_includes.h"


int main(void)
{
	xTaskCreate		(taskGui,
					"main task",
					stackGui,
					NULL,
					tskNORMAL_PRIORITY,
					NULL);

	xTaskCreate		(taskHardwareMeasurement,
					"Hardware measurement",
					stackHardwareMeasurement,
					NULL,
					tskHIGH_PRIORITY,
					NULL);

	xTaskCreate		(taskMeasurement,
					"measurement",
					stackMeasurement,
					NULL,
					tskNORMAL_PRIORITY,
					NULL);

	xTaskCreate		(taskTouchPanel,
					"touch panel",
					stackTouchPanel,
					NULL,
					tskHIGH_PRIORITY,
					NULL);

	xTaskCreate		(taskStatusLed,
					"status led",
					stackStatusLed,
					NULL,
					tskNORMAL_PRIORITY,
					NULL);

	vTaskStartScheduler();
}
